import { chromium } from "playwright";
import { knex } from "../server/knex_config";
import { ProductService } from "./product.service";
import { ScrapService } from "./scrap.service";
import { SearchService } from "./search.service";
import { Top10ProductService } from "./top10product.service";

async function main() {
  let browser = await chromium.launch({ headless: false });
  let productService = new ProductService(knex);
  let top10ProductService = new Top10ProductService(knex);
  let scrapService = new ScrapService(
    knex,
    browser,
    productService,
    top10ProductService
  );
  let searchService = new SearchService(knex, productService, scrapService);
  let result = await searchService.searchProductByKeyword("朱古力", 10);
  console.log({ result });
}
main()
  .then(() => process.exit(0))
  .catch((e) => {
    console.error(e);
    process.exit(1);
  });
