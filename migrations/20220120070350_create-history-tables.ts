import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if(!(await knex.schema.hasTable('text_searching_history'))){
        await knex.schema.createTable('text_searching_history', table=>{
            table.increments('id')
            table.string('searching_word')
            table.integer('frequency')
            table.integer('session_id').defaultTo(0)
            table.timestamps(false,true)
        })
    }


}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('text_searching_history')

}

