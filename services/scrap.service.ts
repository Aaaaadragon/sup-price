import { Knex } from "knex";
import { Browser } from "playwright";
import { setTimeout } from "timers";
import { HKtvmallScraper } from "../scraper/hktvmall-scraper";
import { ParknScraper } from "../scraper/parkn-scraper";
import { Scraper } from "../scraper/scraper";
import { ZtoreScraper } from "../scraper/ztore-scraper";
import { NewProduct, ProductService } from "./product.service";
import { Top10ProductService } from "./top10product.service";

export class ScrapService {
  scraperList: Scraper[] = [];

  constructor(
    knex: Knex,
    browser: Browser,
    productService: ProductService,
    top10ProductService: Top10ProductService
  ) {
    this.scraperList.push(
      new ParknScraper(knex, browser, productService, top10ProductService)
    );
    this.scraperList.push(
      new ZtoreScraper(knex, browser, productService, top10ProductService)
    );
    this.scraperList.push(
      new HKtvmallScraper(knex, browser, productService, top10ProductService)
    );
  }

  async searchAndStore(
    keyword: string,
    numberOfItem: number
  ): Promise<NewProduct[]> {
    let productList: NewProduct[] = [];
    let ps = this.scraperList.map((scraper) =>
      scraper
        .searchAndStore(keyword, numberOfItem)
        .then((result) => productList.push(...result.productList))
    );
    return new Promise((resolve, reject) => {
      Promise.all(ps).then(() => {
        console.log('Searching results from all shop successfully.')
        resolve(productList)});
      setTimeout(() => {
        console.log('Searching timeout, result(s) from some shop are missing...')
        resolve(productList)
      }, 50 * 1000);
    });
  }
}
