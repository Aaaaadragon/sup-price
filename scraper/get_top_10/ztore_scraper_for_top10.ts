import { Top10ProductService } from '../../services/top10product.service'
import { knex } from '../../server/knex_config'
import { Page, chromium } from 'playwright'

const href_txt = "https://www.ztore.com"
export let top10ProductService = new Top10ProductService(knex)

async function main(url: string, returnCount: number = 999,index:number) {
    const browser = await chromium.launch();
    const context = await browser.newContext({
        userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.72 Safari/537.36'
    })
    let page: Page = await context.newPage()

    await page.goto(url)
    console.log('Ztore: scraping for ',urlList[index].category)
    let productList = await page.evaluate(() => {

        function scrollToLast(cb: () => void) {
            let es = document.querySelectorAll('.ProductItem')
            // console.log('number of products:', es.length);
            let last = es[es.length - 1]
            last.scrollIntoView()
            setTimeout(() => {
                let es2 = document.querySelectorAll('.ProductItem')
                if (es2.length != es.length) {
                    return scrollToLast(cb)
                } else {
                    cb()
                }
            }, 500)
        }

        type ProductItem =
            {
                name: string,
                image_src: string,
                price: number,
                href: string
            }
        return new Promise<ProductItem[]>((resolve) => {
            scrollToLast(() => {
                let productList: Array<Promise<ProductItem | null>> = []

                // document.querySelectorAll('.ProductTopSevenTabs .ProductItem').forEach(root => {
                document.querySelectorAll('.ProductItem').forEach(root => {
                    productList.push(new Promise<ProductItem | null>((resolve, reject) => {

                        function loadImage() {
                            // let name = root.querySelector('.name[title]')?.textContent?.trim()
                            let name = root.querySelector('.name[title]')?.getAttribute("title")?.trim()
                            let img = root.querySelector<HTMLImageElement>('.img img')
                            let priceText = root.querySelector('.price .promotion')?.textContent || root.querySelector('.price')?.textContent
                            let href = root.querySelector("a")?.getAttribute("href")
                            // console.log({ name, priceText });

                            if (!name || !img || !priceText || !href) {
                                resolve(null)
                                return
                            }

                            // href = href_txt + href

                            let price = +(priceText.replace('$', '').trim())
                            if (!price) {
                                reject(new Error('failed to parse price'))
                                return
                            }
                            let image_src = img?.src
                            if (image_src.startsWith('data:')) {
                                // console.log('image still loading?:', img);
                                img.scrollIntoView()
                                setTimeout(loadImage, 500)
                            } else {
                                resolve({ name, image_src, price, href })
                            }
                        }

                        loadImage()
                    }))
                })

                Promise.all(productList).then(maybeProductList => {
                    let productList =
                        maybeProductList
                            .filter(product => product != null)
                            .map(product => product!)

                    resolve(productList)
                })
            })
        })
    })
    productList.forEach(item => {
        item.href = href_txt + item.href
    })
    

    browser.close()

    return productList.slice(0, returnCount)

}





export let urlList =
    [
        { category_id: 1, category: "snacks", url: 'https://www.ztore.com/tc/category/group/snacks' },
        { category_id: 2, category: "beverage", url: 'https://www.ztore.com/tc/category/group/beverage' },
        { category_id: 3, category: "alcohol", url: 'https://www.ztore.com/tc/category/group/alcohol' },
        { category_id: 4, category: "grocery", url: 'https://www.ztore.com/tc/category/group/grocery' },
        { category_id: 5, category: "household-supplies", url: 'https://www.ztore.com/tc/category/group/household-supplies' },
        { category_id: 6, category: "dogs", url: 'https://www.ztore.com/tc/category/group/dogs' },
        { category_id: 6, category: "cats", url: 'https://www.ztore.com/tc/category/group/cats' },
    ]



// main()
export async function getItems(urlList: any[]) {
    let totalResult = 0
    let product: any[] = []
    console.log('Ztore: Start scraping')
    for (let i = 0; i < urlList.length; i++) {
        let url = urlList[i].url
        
        let category_id = urlList[i].category_id
        // let productList;
        // let returnCount = (urlList[i].category_id != 6) ? 10 : 5
        try {
            setTimeout(async () => {
                await main(url, 30, i).then(async productListTemp => {
                    let productList = productListTemp
                    
                    productList.forEach(item => {
                        let productObject =
                        {
                            onlineshop: "ztore",
                            title: item.name,
                            price: item.price,
                            item_id: item.href.split("-").slice(-2)[0],
                            category_id: category_id,
                            href: item.href,
                            img_src: item.image_src
                        }
                        product.push(productObject)

                    })
                    console.log('Ztore: finished scraping for ',urlList[i].category)
                    // console.log({ category_id: category_id, catCount: productList.length, totalCount: product.length });
                    if (i == urlList.length - 1) {
                        // console.log(product.length);
                        for (let item of product) {

                            await top10ProductService.createProduct(item)
                        }
                        
                        totalResult += product.length
                        console.log('Ztore:  top 10 items scrapping finished, number of total results:',totalResult)
                    }
                })
            },
                5000 * i
            )
        } catch (err) {
            console.log("error");
        };
    }

    
}






