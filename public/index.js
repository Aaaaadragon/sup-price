let productsTitle = document.querySelector(".top10-title");
let webMainIcon = document.querySelector(".sup-price-logo");
const randomTenProductsTitle = "隨機 10 個商品";



//  Show Product
async function showProducts() {
  for (let onlineshop of ["hktvmall", "Parkn", "ztore"]) {
    let allProducts = await fetch(`/topTenItems/${onlineshop}/1`);
    let showProducts = await allProducts.json();
    //  Clear
    let shop = showProducts[1].onlineshop;
    document.querySelector(`.${shop}`).innerHTML = "";
    //  Show all product
    for (let product of showProducts) {
      let item = product.item_id;
      let title = product.title;
      let price = product.price;
      let href = product.href;
      let photo = product.img_src;

      if (product.onlineshop === "Parkn") {
        photo = photo.slice(0, 7) + "/" + photo.slice(7);
        href = href.slice(0, 7) + "/" + href.slice(7);
      }
      if (product.onlineshop === "hktvmall") {
        let s_location = photo.indexOf("s");
        if (photo[s_location - 1] === "/") {
          photo = photo.slice(0, s_location) + photo.slice(s_location + 1);
        }
      }

      //  Create
      document.querySelector(`.${shop}`).innerHTML += /* html */ `
                <div class="product-card">
                  <a href="${href}">
                    <img class="product-image" src="${photo}">
                  </a>
                  <div class="product-info">
                      <a class="product-title" href="${href}">${title}</a>
                      <div class="product-price">$ ${price}</div>
                  </div>
                </div>
            `;
      // console.log(price, href)
    }
  }
}
showProducts();

//  Switch Category
let selector = document.querySelectorAll(".category");
for (let category of selector) {
  category.addEventListener("change", async (event) => {
    for (let onlineshop of ["hktvmall", "Parkn", "ztore"]) {
      let category_id = event.target.value;
      let allProducts = await fetch(
        `/topTenItems/${onlineshop}/${category_id}`
      );
      let showProducts = await allProducts.json();

      productsTitle.textContent = randomTenProductsTitle

      //  Clear
      let shop = showProducts[1].onlineshop;
      document.querySelector(`.${shop}`).innerHTML = "";
      //  Show all product
      for (let product of showProducts) {
        let item = product.item_id;
        let title = product.title;
        let price = product.price;
        let href = product.href;
        let photo = product.img_src;

        if (product.onlineshop == "Parkn") {
          photo = photo.slice(0, 7) + "/" + photo.slice(7);
          href = href.slice(0, 7) + "/" + href.slice(7);
        }
        if (product.onlineshop == "hktvmall") {
          let s_location = photo.indexOf("s");
          if (photo[s_location - 1] == "/") {
            photo = photo.slice(0, s_location) + photo.slice(s_location + 1);
          }
        }

        //  Create
        document.querySelector(`.${shop}`).innerHTML += /* html */ `
                        <div class="product-card">
                        <a href="${href}">
                          <img class="product-image" src="${photo}">
                        </a>
                            <div class="product-info">
                                <a class="product-title" href="${href}">${title}</a>
                                <div class="product-price">$ ${price}</div>
                            </div>
                        </div>
                    `;
        // console.log(price, href)
      }
    }
  });
}

//  Text Search and Image Search
document
  .querySelector("#search-form")
  .addEventListener("submit", async (event) => {
    event.preventDefault();
    if (document.querySelector("dialog")) {
      document.querySelector("dialog").remove();
    }
    const form = event.target;
    const formData = new FormData();
    if (!form.product.value || form.product.value == " ") {
      form.product.value = "";
      return;
    }
    let keyword = form.product.value;
    // console.log("User search:", keyword);
    formData.append("product ", keyword);

    //  Clear product container
    let row = document.querySelectorAll(".right-col");
    for (let shop of row) {
      shop.innerHTML = "";
    }
    //  Loading Page
    let dialog = document.createElement("dialog");
    dialog.setAttribute("class", "loading");
    dialog.setAttribute("open", true);
    dialog.innerHTML = /*html*/ `
            <div class="loading-container">
              <div class="loading-text"><h1 data-text="Loading...">Loading...</h1></div>
            </div>
             <div class="slider">
               <div class="slider-track">
                 <div class="slide loading-cart"><i class="bi bi-cart4"></i></div>
                 <div class="slide loading-cart">${keyword}</div>
                 <div class="slide loading-cart"><i class="bi bi-cart4"></i></div>   
                 <div class="slide loading-cart">${keyword}</div>
                 <div class="slide loading-cart"><i class="bi bi-cart4"></i></div>  
                 <div class="slide loading-cart">${keyword}</div>
               </div>
             </div>
            `;
    document.body.appendChild(dialog);

    //  fetch to backend
    const res = await fetch(`/search/${keyword}`);
    const result = await res.json();
    // console.log(result);

    // Change products column title
    productsTitle.textContent = `關鍵字 ${keyword} 的搜尋結果`;

    //  Clear Text
    form.product.value = "";
    let imgBtn = document.querySelector(".image");
    if (imgBtn.hasAttribute("open")) {
      imgBtn.setAttribute("hidden", true);
    }

    //  Title change
    let title = (document.querySelector(".top10-title").hidden = false);
    title.innerHTML = "";
    title.innerHTML = /*html*/ `
    搜尋結果
    `;

    //  Close dialog
    dialog.remove();

    //  Search result
    for (let product of result) {
      let shop = product.onlineshop;
      let item = product.item_id;
      let title = product.title;
      let price = product.price;
      let href = product.href;
      let photo = product.img_src;
      if (product.onlineshop === "Parkn") {
        photo = photo.slice(0, 7) + "/" + photo.slice(7);
        href = href.slice(0, 7) + "/" + href.slice(7);
      }
      if (product.onlineshop === "hktvmall") {
        let s_location = photo.indexOf("s");
        if (photo[s_location - 1] === "/") {
          photo = photo.slice(0, s_location) + photo.slice(s_location + 1);
        }
      }

      //  Create
      document.querySelector(`.${shop}`).innerHTML += /* html */ `
        <div class="product-card">
          <a href="${href}">
            <img class="product-image" src="${photo}">
          </a>
          <div class="product-info">
            <a class="product-title" href="${href}">${title}</a>
            <div class="product-price">$ ${price}</div>
          </div>
          <a href="${href}"></a>
        </div>
        `;
    }
    // console.log(price, href)
  });

//  Image Button
document.querySelector(".image").addEventListener("click", async (event) => {
  if (document.querySelector("dialog")) {
    return;
  }
  event.preventDefault();
  let dialog = document.createElement("dialog");
  dialog.setAttribute("class", "image-input");
  dialog.setAttribute("open", "");
  dialog.innerHTML = /*html */ `
    <form class="upload-image">
      <div class="img-preview">
        <img id="blah" src="#" alt="預覽圖片" />
      </div>
      <div class="file-insert">  
      <button id="img-upload-btn"> 上傳圖片</button>
        <input type="file" id="img-upload-input" name="img" accept="image/*" hidden textContent="上傳圖片">
      </div>
      <div class="img-button">
        <button class="btn img-submit img-btn rounded-pill" type="submit" value="submit">提交</button>
        <button class="btn img-cancel img-btn rounded-pill" type="cancel" value="cancel">取消</button>
      </div>  
      <div class="search-result" hidden="true">
      </div>
    </form>
    `;
  document.body.appendChild(dialog);

  document
    .getElementById("img-upload-btn")
    .addEventListener("click", (event) => {
      event.preventDefault();
      event.stopPropagation();
      document.getElementById("img-upload-input").click();
    });

  //  Upload Image
  let form = document.querySelector(".upload-image");
  form.addEventListener("submit", async (e) => {
    if (document.querySelector(".search-result")) {
      document.querySelector(".search-result").innerHTML = "";
    }
    e.preventDefault();
    let body = new FormData();

    if (form.img.value) {
      body.append("predict_photo", form.img.files[0]);
    } else {
      body.append("predict_photo", null);
    }

    //  fetch to backend
    const res = await fetch(`/predict/photo`, {
      method: "POST",
      body: body,
    });
    const result = await res.json();

    let search = document.querySelector(".search-result");
    // search.innerHTML = "";
    search.hidden = false;
    search.innerHTML = /*html */ `<div>識別貨品類別結果: (按下進行可搜尋) </div> <div class='row'>
    <div class='col-6' id="result-col-1"></div>
    <div class='col-6' id="result-col-2"></div>
    </div>`;

    let searchResultCol1 = document.querySelector("#result-col-1");
    let searchResultCol2 = document.querySelector("#result-col-2");

    // console.log(result[0][0].split("、"));
    let wordList = [];
    wordList.push(...result[0][0].split("、"));
    wordList.push(...result[1][0].split("、"));
    wordList.push(...result[2][0].split("、"));
    wordList.push(...result[3][0].split("、"));
    wordList.push(...result[4][0].split("、"));
    wordList.push(...result[5][0].split("、"));
    for (let [index,word] of wordList.entries()) {
      let div = document.createElement("div");
      div.setAttribute("class", "result-row");
      div.innerHTML = /*html*/ `
       <button class="AI-img-result">${word}</button> 
      `;

      if(index<5){
        searchResultCol1.appendChild(div);
      }else{
        searchResultCol2.appendChild(div);
      }
    }

    let results = document.querySelectorAll(".AI-img-result");
    for (let search of results) {
      search.addEventListener("click", async (e) => {
        //  Remove dialog and Clear product container
        let dialog = document.querySelector(".image-input");
        dialog.remove();
        let row = document.querySelectorAll(".right-col");
        for (let shop of row) {
          shop.innerHTML = "";
        }
        // console.log("clicked target", e.target.text);
        let searchingText = e.target.textContent;
        console.log({searchingText})
        //  Loading Page
        let loadingDialog = document.createElement("dialog");
        loadingDialog.setAttribute("class", "loading");
        loadingDialog.setAttribute("open", true);
        loadingDialog.innerHTML = /*html*/ `
        <div class="loading-container">
        <div class="loading-text"><h1 data-text="Loading...">Loading...</h1></div>
        <p class="text-white">相關產品資訊可能一段時間未有更新<br>系統正在獲取最新資訊中...<br>(此過程可能需要1分鐘)</p>
      </div>
       <div class="slider">
         <div class="slider-track">
           <div class="slide loading-cart"><i class="bi bi-cart4"></i></div>
           <div class="slide loading-cart">${searchingText}</div>
           <div class="slide loading-cart"><i class="bi bi-cart4"></i></div>                 
           <div class="slide loading-cart">${searchingText}</div>
           <div class="slide loading-cart"><i class="bi bi-cart4"></i></div>
           <div class="slide loading-cart">${searchingText}</div>               
         </div>
       </div>
`;
        //  animate__animated animate__slideInLeft
        document.body.appendChild(loadingDialog);

        //  fetch to backend
        const res = await fetch(`/search/${searchingText}`);
        const result = await res.json();
        // console.log(result);


        productsTitle.textContent = `關鍵字 ${searchingText} 的搜尋結果`;

        //  Clear Text
        let imgBtn = document.querySelector(".image");
        if (imgBtn.hasAttribute("open")) {
          imgBtn.setAttribute("hidden", true);
        }
        //  Close dialog
        loadingDialog.remove();

        //  Search result
        for (let product of result) {
          let shop = product.onlineshop;
          let item = product.item_id;
          let title = product.title;
          let price = product.price;
          let href = product.href;
          let photo = product.img_src;

          if (product.onlineshop === "Parkn") {
            photo = photo.slice(0, 7) + "/" + photo.slice(7);
            href = href.slice(0, 7) + "/" + href.slice(7);
          }
          if (product.onlineshop === "hktvmall") {
            let s_location = photo.indexOf("s");
            if (photo[s_location - 1] === "/") {
              photo = photo.slice(0, s_location) + photo.slice(s_location + 1);
            }
          }

          //  Create
          document.querySelector(`.${shop}`).innerHTML += /* html */ `
      <div class="product-card">
      <img class="product-image" src="${photo}" href="${href}">
      <div class="product-info">
      <a class="product-title" href="${href}">${title}</a>
      <div class="product-price">$ ${price}</div>
      </div>
      <a href="${href}"></a>
      </div>
      `;
        }
      });
    }
  });

  //  image preview
  let img = dialog.querySelector("#img-upload-input");
  img.addEventListener("change", function (event) {
    // console.log(event.target.files[0]);
    document
      .querySelector("#blah")
      .setAttribute("src", URL.createObjectURL(event.target.files[0]));
  });

  //  Cancel Button
  let cancel = document.querySelector(".img-cancel");
  cancel.addEventListener("click", () => {
    // console.log("clicked cancel button");
    dialog.remove();
  });
});

//  AI camera
// document.querySelector("#ai-search").addEventListener("click", (e) => {
//   e.preventDefault();
//   let dialog = document.createElement("dialog");
//   dialog.setAttribute("class", "webcam");
//   dialog.setAttribute("open", "");
//   dialog.innerHTML = /*html*/ `
//     <video id="webcam" autoplay playsinline width="640" height="480"></video>
//     <canvas id="canvas" class="d-none"></canvas>
//     <audio id="snapSound" src="audio/snap.wav" preload = "auto"></audio>
//   `;

//   const webcamElement = document.getElementById("webcam");
//   const canvasElement = document.getElementById("canvas");
//   const snapSoundElement = document.getElementById("snapSound");
//   const webcam = new Webcam(
//     webcamElement,
//     "user",
//     canvasElement,
//     snapSoundElement
//   );
//   webcam
//     .start()
//     .then((result) => {
//       console.log("webcam started");
//     })
//     .catch((err) => {
//       console.log(err);
//     });
// });
