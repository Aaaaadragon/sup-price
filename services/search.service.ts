import { Knex } from "knex";
import { NewProduct, ProductService } from "./product.service";
// import { ScrapService } from "./scrap.service";

declare module 'express-session' {
  interface SessionData {
      session_id : number | 0
  }
}


// const SECOND = 1000;
// const MINUTE = SECOND * 60;
// const HOUR = MINUTE * 60;
// const DAY = 24 * HOUR;
// let SCRAP_INTERVAL = DAY;
// let SCRAP_INTERVAL = 5 * MINUTE;

export class SearchService {
  constructor(
    private knex: Knex,
    private productService: ProductService,
    // private scrapService: ScrapService
  ) {}
  table() {
    return this.knex("text_searching_history");
  }
  async searchProductByKeyword(
    keyword: string,
    // numberOfItem: number,
    // session_id:number|undefined
  ): Promise<NewProduct[]> {

    // const row = await this.knex("text_searching_history")
    //   //.max("created_at as last_time")
    //   .where("searching_word", keyword)
    //   .orderBy("created_at","desc")
    //   .first();
    // if (row) {
    //   let diff = Date.now() - new Date(row.last_time).getTime();
    //   if (diff < SCRAP_INTERVAL) {
    //     let list = await this.productService.searchProductByKeyword(keyword);
    //     return list;
    //   }
    // }


    // let list = await this.scrapSearchResult(keyword, numberOfItem,session_id!);
    // return list;
    let list = await this.productService.searchProductByKeyword(keyword);
    return list;
  }
  // private async scrapSearchResult(keyword: string, numberOfItem: number,session_id:number) {
  //   let list = await this.scrapService.searchAndStore(keyword, numberOfItem);
  //   await this.table().insert({ searching_word: keyword, 'session_id':session_id });
  //   return list;
  // }
}


