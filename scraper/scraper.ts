import { Knex } from "knex";
import { Browser } from "playwright";
import { NewProduct, ProductService } from "../services/product.service";
import { Top10ProductService } from "../services/top10product.service";

export abstract class Scraper {
  constructor(
    public knex: Knex,
    public browser: Browser,
    public productService: ProductService,
    public top10ProductService: Top10ProductService
  ) {}

  // abstract collectTop10();

  protected abstract search(
    keyword: string,
    numberOfItem: number
  ): Promise<NewProduct[]>;

  async searchAndStore(
    keyword: string,
    numberOfItem: number=999
  ): Promise<{ productList: NewProduct[]; newIdList: number[] }> {
    let productList = await this.search(keyword, numberOfItem);
    let newIdList = await this.productService.createProductBatch(productList);
    return {
      productList,
      newIdList,
    };
  }
}
