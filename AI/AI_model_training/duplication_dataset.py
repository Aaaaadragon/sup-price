# %%
import os
import shutil
# %%
train_dir='../AI_dataset/Product_label_dataset/train'
labelList = os.listdir(train_dir)
labelList.remove('.DS_Store')
print('Total word count:', len(labelList))
# %%

for label in labelList:
    folder = os.path.join(train_dir,label)
    fileList = os.listdir(folder)
    filesNum = len(fileList)
    print({label:filesNum})
    while filesNum<500:
        for file in fileList:
            copyCount = 14
            filePath = os.path.join(folder,file)
            copyPath = os.path.join(folder,'copy_'+str(copyCount)+file)
            if os.path.exists(copyPath):
                copyCount += 1
                copyPath = os.path.join(folder,'copy_'+str(copyCount)+file)
            currentFilesNum = len(os.listdir(folder))
            if currentFilesNum < 500:
                shutil.copy2(filePath,copyPath)
    

# %%
for label in labelList:
    folder = os.path.join(train_dir,label)
    fileList = os.listdir(folder)
    filesNum = len(fileList)
    if(filesNum<500):
        print({label:filesNum})
# %%
