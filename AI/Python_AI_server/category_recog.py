
import tensorflow as tf
import cv2
import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

cate_recog_model = tf.saved_model.load((os.getcwd()+'/recognition_category_model'))

def load_data_as_arr(filePath):
    
    
    print(f'[INFO] processing {filePath}')
    img = cv2.imread(filePath)
    # print('Before processing to RGB:',img)
    # print('Before processing to RGB.shape', np.array(img).shape)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    # print('BGRToRGB : ',img)
    # print('BGRToRGB.shape:', np.array(img).shape)
    img = cv2.resize(img, (160,160))
    # print('After resizing.shape:', np.array(img).shape)
    img = tf.cast(img, tf.float32)
    # print('To float32: ',img)
    img = (img/127.5)-1

    return img

    



def predict_cate(path:str):
    print(path)
    if path.split('\\')[-1][-4:] != '.jpg':
      path += ".jpg"
      
    predict_img = load_data_as_arr(path)
    predict_result = cate_recog_model([predict_img],training=False)
    categories = ['果醬、蜜糖、三文治醬', '乾貨、南貨', '洗衣用品', '即沖飲品', '堅果、種籽', '麵包糕點', '碗碟清潔', '家居必備', '餅乾', '罐裝餐類、包裝餐類', '朱古力、糖果、香口膠', '清潔用品', '亞洲酒類', '飲品', '啤酒、黑啤、蘋果酒', '醬料、調味品、烹調材料', '其他零食', '紅酒', '薯片、脆片', '穀類早餐', '麵']
    result_list = []
    top_3_result = tf.nn.top_k(predict_result[0],k=3).indices.numpy()
    top_3_result = top_3_result.tolist()
    result_in_percentage = tf.nn.softmax(predict_result[0])
    rank_1_percentage = result_in_percentage[top_3_result[0]].numpy().tolist()*100
    rank_2_percentage = result_in_percentage[top_3_result[1]].numpy().tolist()*100
    rank_3_percentage = result_in_percentage[top_3_result[2]].numpy().tolist()*100
    result_list.append([categories[top_3_result[0]],rank_1_percentage])
    result_list.append([categories[top_3_result[1]],rank_2_percentage])
    result_list.append([categories[top_3_result[2]],rank_3_percentage])
    for idx,result in enumerate(result_list):
        print(f'No. {idx+1} result: {result[0]}, matching percentage: {format(result[1],".2f")}%')
    return result_list
      

# predict_cate('AI/AI_dataset/Product_label_dataset/train/果仁/什果仁(罐裝).jpg')
