import multer from "multer";
import path from "path";

export const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve(__dirname, "../AI/Python_AI_server/uploads"));
  },
  filename: function (req, file, cb) {
    // cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`)
    // cb(null, `${file.fieldname}-${Date.now()}+${path.extname(file.originalname)}`)
    cb(null, `${file.fieldname}-${Date.now()}.jpg`);
  },
});

export const upload = multer({ storage });
