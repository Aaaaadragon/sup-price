import path from "path";
import playwright, { Browser, Page } from "playwright";
import { knex } from "../../server/knex_config";
import { ProductService, NewProduct } from "../../services/product.service";
// let index = 0
async function main() {
  let browser = await playwright.chromium.launch({ headless: false });
  const context = await browser.newContext({
    userAgent:
      "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.72 Safari/537.36",
  });
  let page: playwright.Page = await context.newPage();
  
  let urlList = {
    餅乾: "https://www.parknshop.com/zh-hk/snacks-desserts/biscuits/c/030100",
    "朱古力,糖果,香口膠":
      "https://www.parknshop.com/zh-hk/snacks-desserts/chocolates-candies-chewing-gum/c/030200",
    "脆片,薯片":
      "https://www.parknshop.com/zh-hk/snacks-desserts/crisps/c/030300",
    堅果: "https://www.parknshop.com/zh-hk/snacks-desserts/nuts-seeds/c/030400",
    其他: "https://www.parknshop.com/zh-hk/snacks-desserts/other-snacks/c/030500",
  };

  // let cateList = [ '餅乾', '朱古力,糖果,香口膠', '脆片,薯片', '堅果', '其他' ]
  for (let category in urlList) {
    await page.goto(urlList[category]);
    await loadAllItems(browser, page, category);
    // browser.close()
  }
}

main();

export async function loadAllItems(
  browser: Browser,
  page: Page,
  category: string
) {
  try {
    let isShowMoreBtnHidden = await (
      await page.$(".showMore .button")
    )?.isHidden();
    console.log("Is showmore available? answer: ", isShowMoreBtnHidden);
    if (!isShowMoreBtnHidden) {
      console.log("showMore button being clicked...");
      await page.click(".showMore .button");
      await loadAllItems(browser, page, category);
    }
  } catch (error) {
    let productList = [];
    const allItemsContainer = await page.$$(".item .border .padding");
    console.log("Number of all items: ", allItemsContainer.length);
    for (let itemInfo of allItemsContainer) {
      let title = await (await itemInfo.$(".name"))?.textContent();
      title = title?.replace(/\s/g, "");
      // console.log('itemInfo name: ' + title)

      let price = await (
        await itemInfo.$(".price-container .display-price .discount")
      )?.textContent();
      price = price?.replace("HK$", "").replace(",", "");
      // console.log('itemInfo price: ' + price)

      let href = await (
        await itemInfo.$(".productPrimaryPic a")
      )?.getAttribute("href");
      href = path.join("https://www.parknshop.com/", href as string);
      // console.log('itemInfo href: '+href)

      let item_id = href?.split("/").slice(-1)[0];

      let imgSrc = await (
        await itemInfo.$(".productPrimaryPic a img")
      )?.getAttribute("data-original");
      imgSrc = path.join("https://www.parknshop.com", imgSrc as string);
      // console.log('itemInfo img source: '+imgSrc)

      if (!title || !price || !href || !imgSrc) {
        continue;
      }
      // console.log({ 'onlineshop': 'Parkn', 'title': title, 'price': +price, 'item_id': item_id, 'category_id': 1, 'href': href, 'img_src': imgSrc ,'is_training_data':true})
      productList.push({
        onlineshop: "Parkn",
        title: category + "-" + title,
        price: +price,
        item_id: item_id,
        category_id: 1,
        href: href,
        img_src: imgSrc,
        is_training_data: true,
      });
    }
    insertProductsToSQL(productList)
    // console.log('ERROR: ',error)
    
  }
}

export async function insertProductsToSQL(productList: NewProduct[]) {
  let productService = new ProductService(knex);
  productService.createProductBatch(productList);
}
