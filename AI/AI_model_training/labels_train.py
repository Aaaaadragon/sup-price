# %% import library
import os
import cv2
import tensorflow as tf
import numpy as np
import random
import matplotlib.pyplot as plt
# %% load label list
train_dir='../AI_dataset/Product_label_dataset/train'
all_labels = os.listdir(train_dir)
all_labels.remove('.DS_Store')
print('Total word count:',len(all_labels))
# %% quick scan dataset
skip_labels = []
nonskip_labels = []
label_counts = []
min_count = 400
max_count = 800
for label in all_labels:
    files = os.listdir(os.path.join(train_dir,label))
    count = len(files)
    if count > max_count:
        count = max_count
    label_counts.append(count)
    print(label, count)
    if count < min_count:
        skip_labels.append(label)
    else:
        nonskip_labels.append(label)
plt.bar(
    x=list(range(len(all_labels))),
    height=label_counts
)
plt.show()
print('nonskip labels:', len(nonskip_labels))
print('nonskip labels:', nonskip_labels)
print('nonskip labels:', label_counts)
# %% config image size
image_width = 160
image_height = 160
image_size = (image_width, image_height)
# %% load dataset
all_images = []
all_labels = []
label_counts = []
labels = nonskip_labels
for idx, label in enumerate(labels):
    print('scan label:', label)
    files = os.listdir(os.path.join(train_dir,label))
    count = len(files)
    label_counts.append(count)
    i = 0
    for file in files:
        i = i + 1
        if i > max_count:
            break
        if not file.endswith('.jpg'):
            continue
        file = os.path.join(train_dir,label,file)
        # print('load image:', file)
        try:
            img = cv2.imread(file)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            img = cv2.resize(img, image_size)
            img = tf.convert_to_tensor(img, dtype=tf.float32)
            img = (img / 127.5) - 1
            all_images.append(img)
            all_labels.append(idx)
        except:
            print('failed to load image:', file)
            os.unlink(file)
plt.bar(
    x=list(range(len(labels))),
    height=label_counts
)
plt.show()
# %%
X = np.array(all_images, dtype=np.float32)
Y = np.array(all_labels).reshape(len(X),1)
# %%
print('X:', X.shape)
print('Y:', Y.shape)
# %%
base_model = tf.keras.applications.MobileNetV2(
    input_shape=(image_width, image_height, 3),
    include_top=False,
    weights='imagenet',
    pooling='avg'
)
base_model.trainable = False
base_model.summary()
# %%
# len(labels)
# %%
model = tf.keras.Sequential()
model.add(base_model)
# model.add(tf.keras.layers.Dropout(0.3))
model.add(tf.keras.layers.BatchNormalization())
model.add(tf.keras.layers.Dense(len(labels)))
model.add(tf.keras.layers.Softmax())

model.summary()
# %%
train_learning_rate = 0.005
model.compile(
    optimizer=tf.keras.optimizers.RMSprop(lr=train_learning_rate),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=['accuracy']
)
# %%
# model(np.random.random(160*160*3*10).reshape(10,160,160,3), training=False)
# %%
train_X = []
train_Y = []
test_X = []
test_Y = []
validation_split = 0.1
for i in range(len(X)):
    if random.random() < validation_split:
        test_X.append(X[i])
        test_Y.append(Y[i])
    else:
        train_X.append(X[i])
        train_Y.append(Y[i])
train_X = np.array(train_X)
train_Y = np.array(train_Y)
test_X = np.array(test_X)
test_Y = np.array(test_Y)
print('train:', train_X.shape)
print('test:', test_X.shape)
# %%
history = model.fit(
    epochs=10,
    x=train_X,
    y=train_Y,
    validation_data=(test_X, test_Y),
    # validation_split=0.1,
    shuffle=True,
    batch_size=100,
    use_multiprocessing=True,
    workers=8
)
# %%
acc = history.history['accuracy']

val_acc = history.history['val_accuracy']

loss = history.history['loss']
val_loss = history.history['val_loss']

plt.figure(figsize=(8, 8))
plt.subplot(2, 1, 1)
plt.plot(acc, label='Training Accuracy')
plt.plot(val_acc, label='Validation Accuracy')
plt.legend(loc='lower right')
plt.ylabel('Accuracy')
plt.ylim([min(plt.ylim()),1])
plt.title('Training and Validation Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss, label='Training Loss')
plt.plot(val_loss, label='Validation Loss')
plt.legend(loc='upper right')
plt.ylabel('Cross Entropy')
plt.ylim([0,1.0])
plt.title('Training and Validation Loss')
plt.xlabel('epoch')
plt.show()
# %%
model.save('../Python_AI_server/recognition_label_model',overwrite=True);
# %%
