import fs from "fs";
import fetch from "node-fetch";
import { knex } from "../server/knex_config";
import { ProductService } from "../services/product.service";

export async function download(url: string, path: string) {
  const response = await fetch(url);
  const buffer = await response.buffer();
  fs.writeFile(path, buffer, () => console.log("finished downloading!" + path));
}

async function main() {
  let productService = new ProductService(knex);
  const itemsInfo = await productService.getAllTrainingData();
  console.log(itemsInfo.length);

  // let folder = "./ztore_image/";
  // let folder= "C:/Users/samue/sup-price/AI_data_scraper/ztore_image/"
  //TODO git ignore the image download folder
  for (let i = 0; i < itemsInfo.length; i++) {
    let filename = itemsInfo[i].title;
    let className = filename.split('-')[0]
    let img_src = itemsInfo[i].img_src;

    fs.mkdirSync(`C:/Users/samue/sup-price/AI_data_scraper/ztore_image/${className}`,{recursive:true})
    if(fs.existsSync(`C:/Users/samue/sup-price/AI_data_scraper/ztore_image/${className}/${filename}.jpg`)){
      console.log(`${filename}.jpg already exist.`)
      continue
    }

    let urlTrim = img_src.replace("https://www.ztore.com/_next/image?url=", "");
    let download_url = "https://images.weserv.nl/?w=160&h=160&url=" + urlTrim;
    let save_path = `C:/Users/samue/sup-price/AI_data_scraper/ztore_image/${className}/` + filename + ".jpg";
    // console.log({url, path});

    await download(download_url, save_path);
  }
  knex.destroy()
}

main();
