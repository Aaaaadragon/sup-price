import { chromium } from "playwright";
import { knex } from "../../server/knex_config";
import { ProductService } from "../../services/product.service";
import { Top10ProductService } from "../../services/top10product.service";
import { HKtvmallScraper } from "../hktvmall-scraper";

async function main() {
  let productService = new ProductService(knex);
  let top10ProductService = new Top10ProductService(knex);
  let browser = await chromium.launch({ headless: false });
  let scraper = new HKtvmallScraper(
    knex,
    browser,
    productService,
    top10ProductService
  );
  let result = await scraper.searchAndStore("膠囊");
  console.log(
    "found",
    result[0].length,
    "results",
    "inserted",
    result[1].length,
    "results"
  );

  browser.close();
  knex.destroy();
}

main();
