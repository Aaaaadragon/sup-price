import { chromium } from "playwright";
import { Top10ProductService } from "../../services/top10product.service";
import { knex } from "../../server/knex_config";

let sleep = async function (time: number) {
  await timeout(time);
};

async function timeout(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export async function scraper() {
  let top10ProductService = new Top10ProductService(knex);
  let browser = await chromium.launch();
  let itemsInfo = [];
  //  Browser by
  let context = await browser.newContext({
    userAgent:
      "Mozilla / 5.0(Windows NT 10.0; Win64; x64; rv: 96.0) Gecko/20100101 Firefox/96.0",
  });
  let page = await context.newPage();

  //  Goto HKTV mall snack page
  await page.goto("https://www.hktvmall.com/hktv/zh/supermarket");

  console.log("HKtvmall: Try to get product Link");

  // Collect Class list
  let classList = await page.$$(".subnav > ul > li > a");
  let classesInfo = {};
  for (let item of classList) {
    //  Reject unacceptable item
    if (!(await item.getAttribute("data-cat"))) {
      continue;
    }
    let name = (await item.textContent())?.replace(/\s/g, "");
    let href = await item.getAttribute("href");
    // console.log('Item Href :', (await item.getAttribute('href')))
    classesInfo[name as string] = "https://www.hktvmall.com" + href;
  }
  let productInfo;

  for (let className in classesInfo) {
    let category_id;
    switch (className) {
      // case "重量級推介":
      //     break
      case "零食甜品":
        category_id = 1;
        break;
      case "飲品即沖飲品":
        category_id = 2;
        break;
      case "酒類":
        category_id = 3;
        break;
      case "米食油":
      case "調味醬料":
      case "罐頭乾貨":
      case "即食麵麵意粉":
      case "湯熟食醃製食品":
      case "早餐果醬":
      case "有機健康食品":
        category_id = 4;
        break;
      case "紙品即棄品家居用品":
      case "家居清潔用品":
        category_id = 5;
        break;
      case "貓狗用品":
        category_id = 6;
        break;
      default:
        continue;
    }
    console.log('HKtvmall: Scraping items under class',className)
    await page.goto(classesInfo[className]);
    await sleep(1500);
    //  Wait For Browser Load product
    await page.waitForSelector(
      "#search-result-wrapper-outer .product-brief-list"
    );
    let productList = await page.$$(
      "#search-result-wrapper-outer .product-brief"
    );

    // Show 10 Product
    for (let i = 0; i < 60; i++) {
      if (category_id == 4 && i == 9) {
        break;
      }
      if (category_id == 5 && i == 30) {
        break;
      }
      let name = await (
        await productList[i].$(".brand-product-name")
      )?.textContent();
      name?.toString();
      let price = await (await productList[i].$(".price"))?.textContent();
      price = price?.replace("$ ", "");
      let img_src = await (
        await productList[i].$(".square-wrapper img")
      )?.getAttribute("src");

      let href = await (await productList[i].$("> a"))?.getAttribute("href");
      href = "https://www.hktvmall.com/hktv/zh/" + href;
      let item_id = href?.split("/").slice(-1)[0];

      productInfo = {
        onlineshop: "hktvmall",
        title: name!,
        price: +price!,
        item_id: item_id,
        category_id: category_id,
        href: href,
        img_src: img_src!,
      };
      itemsInfo.push(productInfo);
      // console.log(className, itemsInfo)
    //   await top10ProductService.createProduct(productInfo);
    //   console.log(productInfo.category_id);
    }
    console.log('HKtvmall: Finished scraping for',className,'items')
  }
  const idList = await top10ProductService.createProductBatch(itemsInfo)
  console.log('HKtvmall:  top 10 items scrapping finished, number of total results:',idList.length)
//   return itemsInfo;
}
