let productList= []
        document.querySelectorAll('.ProductItem').forEach(root => {
            let name = root.querySelector('.name[title]')?.textContent?.trim()
            let image_src = root.querySelector('.img img')?.src
            let priceText = root.querySelector('.price .promotion')?.textContent || root.querySelector('.price')?.textContent
            console.log({ name, image_src, priceText });
            if (!name || !image_src || !priceText) return
            let price = +priceText.replace('$', '').trim()
            if (!price) { throw new Error('failed to parse price') }
            productList.push({ name, image_src, price })
        })