import { Request, Response } from "express";
import { HttpError } from "../http-error";
import { SearchService } from "../services/search.service";
import { RestController } from "./rest.controller";

export class SearchController extends RestController {
  constructor(private searchService: SearchService) {
    super();
    this.routes.get("/search/:search_text", this.handleRequest(this.search));
  }

  search = async (req: Request, res: Response) => {

    let {search_text} = req.params;
    if (!search_text) {
      throw new HttpError(401, "請輸入關鍵字以作搜尋");
    }
    search_text = decodeURI(search_text);
    //console.log("Searching:", search_text);
    return this.searchService.searchProductByKeyword(search_text);
  };
}
