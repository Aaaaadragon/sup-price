import os
from sanic import Sanic
from sanic.response import json
from label_recog import predict_label
from category_recog import predict_cate

app = Sanic('nft-search')

@app.route('/')
def intro(request):
    return json({"message":"Hello from sup-price"})

@app.post('/predict/category')
def predict_cate_route(request):
    body = request.json
    print(body)
    img_name = body['filename']
    print(img_name)
    result_list_cate= predict_cate(f'{os.getcwd()}/uploads/{img_name}')
    result_list_label=predict_label(f'{os.getcwd()}/uploads/{img_name}')
    result_list = []
    for result in result_list_cate:
        result_list.append(result)
    for result in result_list_label:
        result_list.append(result)
    print(result_list)
    return json(result_list)
if __name__ =="__main__":
    app.run(host="localhost",port=8200)