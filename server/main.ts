import express from "express";
import { print } from "listening-on";
import session from "express-session";
import dotenv from "dotenv";
import { topTenItemsRoutes } from "./top_10_items";
import { SearchController } from "../controller/search.controller";
import { getServices } from "../services/services";
import { PredictController } from "../controller/predict.controller";
import { knex } from "./knex_config";
dotenv.config();

let app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));


if (!process.env.SESSION_SECRET) {
	throw new Error('missing SESSION_SECRET in env')
}


app.use(session({
	secret: process.env.SESSION_SECRET,
	resave: true,
	saveUninitialized: true,
}))

if (!process.env.SESSION_SECRET) {
  throw new Error("missing SESSION_SECRET IN env");
}

app.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: true,
    saveUninitialized: true,
  })
);

app.use(express.static("public"));


app.use(topTenItemsRoutes);

app.get('/',async (req,res)=>{
  if(req.session.session_id == 0 || req.session.session_id == undefined){
    let session_id_record = await knex('text_searching_history').max('session_id as session_id').first()
    if(req.session.session_id != session_id_record?.session_id){
      req.session.session_id = session_id_record?.session_id+1
    }
    req.session.save()
  }
})


getServices({ headless: true }).then((services) => {
  app.use(new SearchController(services.searchService).routes);
  app.use(new PredictController(services.predictService).routes)
});




let port = process.env.PORT;

app.listen(port, () => {
  print(parseInt(port as string));
});
