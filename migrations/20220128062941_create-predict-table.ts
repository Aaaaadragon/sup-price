import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if(!(await knex.schema.hasTable('predict_history'))){
        await knex.schema.createTable('predict_history', table=>{
            table.increments('id')
            table.string('predict_photo')
            table.string('result')
            table.float('accuracy')
            table.timestamps(false,true)
        })
    }
    
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('predict_history')
}

