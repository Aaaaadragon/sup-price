import dotenv from 'dotenv'
import Knex from 'knex'
dotenv.config()
const knexConfig = require('../knexfile')
export const knex = Knex(knexConfig[process.env.NODE_ENV || 'development'])