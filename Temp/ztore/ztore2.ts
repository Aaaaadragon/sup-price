
// import path from 'path';
import { Page, chromium } from 'playwright'

const href_txt = "https://www.ztore.com"

async function main(url: string, returnCount: number = 999) {
    const browser = await chromium.launch();
    const context = await browser.newContext({
        userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.72 Safari/537.36'
    })
    let page: Page = await context.newPage()

    await page.goto(url)

    let productList = await page.evaluate(() => {

        function scrollToLast(cb: () => void) {
            let es = document.querySelectorAll('.ProductItem')
            console.log('number of products:', es.length);
            let last = es[es.length - 1]
            last.scrollIntoView()
            setTimeout(() => {
                let es2 = document.querySelectorAll('.ProductItem')
                if (es2.length != es.length) {
                    return scrollToLast(cb)
                } else {
                    cb()
                }
            }, 500)
        }

        type ProductItem =
            {
                name: string,
                image_src: string,
                price: number,
                href: string
            }
        return new Promise<ProductItem[]>((resolve) => {
            scrollToLast(() => {
                let productList: Array<Promise<ProductItem | null>> = []

                // document.querySelectorAll('.ProductTopSevenTabs .ProductItem').forEach(root => {
                document.querySelectorAll('.ProductItem').forEach(root => {
                    productList.push(new Promise<ProductItem | null>((resolve, reject) => {

                        function loadImage() {
                            let name = root.querySelector('.name[title]')?.textContent?.trim()
                            let img = root.querySelector<HTMLImageElement>('.img img')
                            let priceText = root.querySelector('.price .promotion')?.textContent || root.querySelector('.price')?.textContent
                            let href = root.querySelector("a")?.getAttribute("href")
                            // console.log({ name, priceText });

                            if (!name || !img || !priceText || !href) {
                                resolve(null)
                                return
                            }

                            // href = href_txt + href

                            let price = +(priceText.replace('$', '').trim())
                            if (!price) {
                                reject(new Error('failed to parse price'))
                                return
                            }
                            let image_src = img?.src
                            if (image_src.startsWith('data:')) {
                                // console.log('image still loading?:', img);
                                img.scrollIntoView()
                                setTimeout(loadImage, 500)
                            } else {
                                resolve({ name, image_src, price, href })
                            }
                        }

                        loadImage()
                    }))
                })

                Promise.all(productList).then(maybeProductList => {
                    // let productList = maybeProductList =
                    let productList =
                        maybeProductList
                            .filter(product => product != null)
                            .map(product => product!)
                    // console.log('resolve:', productList);
                    resolve(productList)
                })
            })
        })
    })
    productList.forEach(item => {
        item.href = href_txt + item.href
    })
    // console.log({ productList, productCount: productList.length });

    browser.close()

    // function shuffleArray(array: any) {
    //     for (let i = array.length - 1; i > 0; i--) {
    //         const j = Math.floor(Math.random() * (i + 1));
    //         [array[i], array[j]] = [array[j], array[i]];
    //     }
    // }
    // shuffleArray(productList)

    return productList.slice(0, returnCount)

}





let urlList =
    [
        // { category_id: ,category: "featured-products", url: 'https://www.ztore.com/tc/category/group/featured-products' },
        { category_id: 1, category: "snacks", url: 'https://www.ztore.com/tc/category/group/snacks' },
        { category_id: 2, category: "beverage", url: 'https://www.ztore.com/tc/category/group/beverage' },
        { category_id: 3, category: "alcohol", url: 'https://www.ztore.com/tc/category/group/alcohol' },
        { category_id: 4, category: "grocery", url: 'https://www.ztore.com/tc/category/group/grocery' },
        { category_id: 5, category: "household-supplies", url: 'https://www.ztore.com/tc/category/group/household-supplies' },
        // { category_id: 5,category: "home-kitchen", url: 'https://www.ztore.com/tc/category/group/home-kitchen' },
        { category_id: 6, category: "dogs", url: 'https://www.ztore.com/tc/category/group/dogs' },
        { category_id: 6, category: "cats", url: 'https://www.ztore.com/tc/category/group/cats' },
        // { category_id: ,category: "vouchers", url: 'https://www.ztore.com/tc/category/group/vouchers' },
        // { category: "合味道杯麵", url: 'https://www.ztore.com/tc/search/%E5%90%88%E5%91%B3%E9%81%93%E6%9D%AF%E9%BA%B5?query=%E5%90%88%E5%91%B3%E9%81%93%E6%9D%AF%E9%BA%B5' },
        // { category: "卡樂B 熱浪薯片", url: 'https://www.ztore.com/tc/search/%E5%8D%A1%E6%A8%82B%20%E7%86%B1%E6%B5%AA%E8%96%AF%E7%89%87?query=%E5%8D%A1%E6%A8%82B%20%E7%86%B1%E6%B5%AA%E8%96%AF%E7%89%87' }
    ]

// let searchTxt = "%E5%90%88%E5%91%B3%E9%81%93%E6%9D%AF%E9%BA%B5"
// searchTxt = "cocacola"
// // searchTxt = "卡樂B%20熱浪薯片"
// let searchStr = "卡樂B 熱浪薯片 零食"
// searchStr = "卡樂B 熱浪薯片"
// searchStr = "脆之豆 卡樂B 原味"

// searchTxt = searchStr.split(" ").join("%20")
// let searchQuery = "https://www.ztore.com/tc/search/" + searchTxt + "?query=" + searchTxt
// console.log(searchQuery);
// urlList.push({ category: searchStr, url: searchQuery })

// main()
let product = []
for (let i = 0; i < urlList.length; i++) {
    let url = urlList[i].url
    let category_id = urlList[i].category_id
    // let productList;
    let returnCount = (urlList[i].category_id != 6) ? 10 : 5
    try {
        setTimeout(async () => {
            await main(url, returnCount).then(productListTemp => {
                let productList = productListTemp

                // console.log({ productList, productTotal: productList.length })
                productList.forEach(item => {
                    let productObject =
                    {
                        onlineshop: "ztore",
                        title: item.name,
                        price: item.price,
                        item_id: item.href.split("-").slice(-2)[0],
                        category_id: category_id,
                        href: item.href,
                        img_src: item.image_src
                    }
                    product.push(productObject)
                    // console.log(productObject);

                })
                console.log({ category_id: category_id, catCount: productList.length, totalCount: product.length });
                if (i == urlList.length - 1) {
                    console.log(product.length);
                    // SQL insert into database 

                }
                // console.log({ product: productList, category: urlList[i].category, count: productList.length })
                // product.push({ product: productList, category: urlList[i].category, count: productList.length })
            })
            // console.log({ productCount: productList.length });
        },
            5000 * i
        )
    } catch (err) {
        console.log("error");
    };
}


// console.log(product.length);
// setInterval(()=>console.log(product.length), 10000)