import {Knex} from 'knex'
import { HttpError } from '../http-error'

export async function seed(knex:Knex) {

   async function seedCategory(row:{id?:number,name:string}) {
       try {
           if(!(await knex('category').select('name').where({id:6}).first())){
            throw new HttpError(404,'category not found')
           }
       } catch (error) {
           await knex('category').insert(row)
       }
   }

   await seedCategory({id:0,name:'無分類'})
   await seedCategory({name:'零食'})
   await seedCategory({name:'飲品'})
   await seedCategory({name:'酒類'})
   await seedCategory({name:'糧油雜貨'})
   await seedCategory({name:'家居用品'})
   await seedCategory({name:'寵物專區'})

}