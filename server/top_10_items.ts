import express from 'express'
import { knex } from './knex_config'
import { Top10ProductService } from '../services/top10product.service'
export let topTenItemsRoutes = express.Router()

topTenItemsRoutes.get('/topTenItems/:shopName/:category', async (req, res) => {
    try {
        if (!req.params.shopName) {
            res.status(401).json({ message: "請提供網店名稱" })
        }
        if (!req.params.category) {
            res.status(401).json({ message: "請提供類別" })
        }
        const shopName = req.params.shopName
        const category = req.params.category
        // console.log(shopName, category)
        const topTenItemsRoutes = new Top10ProductService(knex)
        const result = await topTenItemsRoutes.get10RandomProductByCategory(shopName, +category)
        // console.log(result)
        res.json(result)
    } catch (error) {
        res.status(500).json({ message: error })

    }
})