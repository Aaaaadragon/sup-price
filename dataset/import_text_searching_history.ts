import path from "path";
import fs from 'fs'
import { knex } from "../server/knex_config";

export async function importSearchHistory() {
    let words = fs.readFileSync(path.join(__dirname,'finished_download_word.txt'),{encoding:"utf-8"})
    let wordList = words.split('\n')
    for (let word of wordList){
        await knex('text_searching_history').insert({searching_word:word,'session_id':0})
    }
    console.log('Insert word searching history data finished!')
}



