import { knex } from "../server/knex_config";
import { PredictService } from "./predict.service";
import { ProductService } from "./product.service";
import { SearchService } from "./search.service";
// import { chromium } from "playwright";
// import { ScrapService } from "./scrap.service";
// import { Top10ProductService } from "./top10product.service";

export async function getServices(options: { headless: boolean }) {
  // let browser = await chromium.launch({ headless: options.headless });
  let productService = new ProductService(knex);
  // let top10ProductService = new Top10ProductService(knex);
  // let scrapService = new ScrapService(
  //   knex,
  //   browser,
  //   productService,
  //   top10ProductService
  // );
  let searchService = new SearchService(knex, productService);
  let predictService = new PredictService(knex)
  return {
    searchService,predictService
  };
}
