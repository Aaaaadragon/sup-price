import { Knex } from "knex"
import fetch from 'node-fetch'


export type predict_result = [
    result:string,matching_percentage:number
]

export class PredictService{
    predict_cate(photo_name: string) {
        throw new Error("Method not implemented.")
    }
    
    constructor(private knex:Knex){}

    table() {
        return this.knex('predict_history')
    }

    async predict(filename:string):Promise<predict_result[]>{
        // console.log({filename});
        
        let res = await fetch('http://localhost:8200/predict/category',{
            method:'POST',
            headers:{
                "content-type":"application/json"
            },
            body:JSON.stringify({filename})
        })
        let result = await res.json()
        // let result_list = result.result_list
        console.log(result)
        // console.log({result})
        // console.log(result.result_cate)
        // console.log(result.matching_percentage)
        await this.table().insert({'predict_photo':filename,'result':result[0][0],'accuracy':+(result[0][1]).toFixed(1)})
        await this.table().insert({'predict_photo':filename,'result':result[3][0],'accuracy':+(result[3][1]).toFixed(1)})
        return new Promise((resolve,reject)=>{
            resolve(result)
        })
    }
    
}