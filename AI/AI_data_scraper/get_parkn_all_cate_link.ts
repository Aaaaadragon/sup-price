import playwright from 'playwright'
import path from "path";
import { ProductService } from '../services/product.service';
import { Top10ProductService } from '../services/top10product.service';
import { knex } from '../server/knex_config';
import { ParknScraper } from '../scraper/parkn-scraper';
import fetch from 'node-fetch'
import fs from 'fs'

main()

async function main() {

    

    const browser = await playwright.chromium.launch({headless:true});
    const context = await browser.newContext({
        userAgent:'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36'
    })
    let page = await context.newPage()

    let productService = new ProductService(knex)
    let top10ProductService = new Top10ProductService(knex)
    let parnkScraper = new ParknScraper(
        knex,
        browser,
        productService,
        top10ProductService
    )

    let classesInfo = await getAllClasses(page, 'https://www.parknshop.com/zh-hk/')
    console.log(classesInfo)
    let allLinkInfoList = []
    for (let title in classesInfo) {
        let category_id;
        switch (title) {
            case '餅乾、零食、糖果':
                category_id = 1
                continue
                break
            case '飲品、即沖飲品':
                category_id = 2
                continue
                break
            case '酒類':
                category_id = 3
                continue
                break
            case '米、麵、食油':
                category_id = 4
                continue
                
            case '罐頭、醬料、湯': 
                category_id = 4
                continue

            case '早餐及果醬':
                continue     
                category_id = 4
                break
            case '紙巾、廁紙': 
                category_id = 4
                continue
                break
            
            case '家居清潔':
                category_id = 5
                break
            case '寵物食品及護理':
                category_id = 6
                continue
                break
            default:
                continue
        }

        // let link = classesInfo[title]
        console.log('scraping for ...', title,'category_id',category_id)
        await page.goto(classesInfo[title])
        const allItemsContainer = await page.$$('.detail')
        
        for(let itemsContainer of allItemsContainer ){
            let className = await(await itemsContainer.$('.clearfix .title a'))?.textContent()
            if(!className){continue}
            let linkToShowAll = await (await itemsContainer.$('.btn-show-all'))?.getAttribute('href')
            if(!linkToShowAll){continue}
            linkToShowAll = path.join('https://www.parknshop.com/',linkToShowAll)
            allLinkInfoList.push({[className]:linkToShowAll})
        }
        
    }
    console.log(allLinkInfoList)
        for(let linkInfo of allLinkInfoList){
            let className = Object.keys(linkInfo)[0]
            let link = Object.values(linkInfo)[0]
            let folderPath = path.join('/Users/aaadragon/Desktop/sup-price/AI_dataset/Parkn_dataset',className)
            fs.mkdirSync(folderPath, { recursive: true });
            await page.goto(link)
            let productList = await parnkScraper.collectProductList(page,999,true)
            console.log({ProductListLength:productList.length})
            await productService.createProductBatch(productList)
            for(let product of productList){
                let title = product.title
                let imgSrc = product.img_src
                let imgPath = path.join(folderPath,title+'.jpg')
                if(fs.existsSync(imgPath)){
                    console.log(imgPath,'already exist')
                    continue
                }
                imgSrc = imgSrc.slice(0,7) + '/' + imgSrc.slice(7)
                let resizedImgSrc = 'https://images.weserv.nl/?w=160&h=160&url=' + imgSrc
                console.log('Downloading',title+'.jpg','\nSaving to',imgPath)
                await downloadImg(resizedImgSrc,title,imgPath)
            }
        }
    browser.close()
}


export async function getAllClasses(page: playwright.Page, pageLink: string) {
    let classesInfo = {}
    await page.goto(pageLink)
    const allClassesLabel = await page.$$('.category-title a')
    for (let classLabel of allClassesLabel) {
        let title = await (await classLabel.$('.text'))?.textContent()
        let href = await classLabel.getAttribute('href')
        classesInfo[title as string] = path.join('https://www.parknshop.com', href as string)
    }
    return classesInfo
}


export async function downloadImg(imgSrc:string,imgTitle:string,imgPath:string) {
    const response = await fetch(imgSrc);
    console.log('Fetching from',imgSrc)
    const buffer = await response.buffer();
  //   const arrayBufferView = new Uint16Array(buffer)
    fs.writeFile(imgPath, buffer, () => 
      console.log(imgTitle+'.jpg','finished downloading!'));
  }

function timeout(ms: number | undefined) {
    return new Promise(resolve => setTimeout(resolve, ms))
}

export async function sleep(seconds: number) {
    await timeout(seconds)
}
