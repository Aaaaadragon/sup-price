import {scraper as hktvmall_scraper_for_top10} from './hktvmall_scraper_for_top10'
import {scraper_parkn as parkn_scraper_for_top10} from './parkn_scraper_for_top10'
import {getItems as ztore_scraper_for_top10, urlList} from './ztore_scraper_for_top10'

async function main() {
    console.log('------------------------------Start to scraping from hktvmall/parkn/ztore---------------------------')
    hktvmall_scraper_for_top10()
    // console.log('------------------------------Start to scraping from parkn------------------------------')
    parkn_scraper_for_top10()
    // console.log('------------------------------Start to scraping from ztore------------------------------')
    ztore_scraper_for_top10(urlList)
    // console.log('------------------------------Finished all top 10 items scraping------------------------')
}

main()
