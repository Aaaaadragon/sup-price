import { Knex } from "knex";
import { HttpError } from "../http-error";

export type NewTop10Product = {
  onlineshop: string;
  title: string;
  price: number;
  item_id: string;
  category_id: number;
  href: string;
  img_src: string;
};

export class Top10ProductService {
  constructor(private knex: Knex) { }

  table() {
    return this.knex("top_10_items");
  }

  async createProduct(row: NewTop10Product) {
    const onlineshop = row.onlineshop;
    const item_id = row.item_id;
    let result = await this.table().select("id").where({ onlineshop, item_id });
    if (result) {
      console.log("Item_id:", row.item_id, "already exist.\ntry to update...");
      this.table().where('item_id','=',row.item_id).update(row)
      console.log("PROCESS <createProduct> finished")
      return;
    }
    // console.log("createProduct:", row);
    let rows: { id: number }[] = await this.table().insert(row).returning("id");
    console.log("PROCESS <createProduct> finished")
    return rows[0];
  }
  async createProductBatch(rows: NewTop10Product[]) {
    // console.log("Total item number", rows.length);
    let updateItemList: NewTop10Product[] = [];
    for (let item of rows) {
      let result = await this.table()
        .select("id")
        .where("item_id", item.item_id);

      // console.log('database select same item_id result:',result)

      if (result[0]) {
        // console.log("Item_id:", item.item_id, "already exist.\ntry to update...");
        this.table().where('item_id','=',item.item_id).update(item)
        updateItemList.push(item);
      }
    }

    rows = rows.filter((item) => !updateItemList.includes(item));
    if (rows[0] == undefined) {
      console.log("All of the results product has been created before...\ntry to update info of products...");
      for(let item of updateItemList){
        this.table().where('item_id','=',item.item_id).update(item)
      }
      console.log("Item to be updated:", updateItemList.map((item)=>item.title))
      console.log("PROCESS <createProductBatch> finished")
      return [];
    }
    
    console.log("Number of new/updated Products to be inserted:", rows.length);
    console.log("Item to be updated:", updateItemList.map((item)=>item.title))
    
    console.log("Selection finished, inserting to database...");
    let resultRows: { id: number }[] = await this.table()
    .insert(rows)
    .returning("id");
    
    console.log("PROCESS <createProductBatch> finished")
    return resultRows.map((row) => row.id);
  }


  
  async get10RandomProductByCategory(onlineshop: string, category_id: number) {
    //let rows = (await this.table().select('*').where({ 'onlineshop': onlineshop, 'category_id': category_id })).slice(0, 10)
    let rows =  await this.table().select('*').where({ 'onlineshop': onlineshop, 'category_id': category_id }).limit(10).offset(0).orderBy("id","desc");
    // console.log(rows.toSQL())
    if (rows.length  == 0) {
      throw new HttpError(404, 'product not found')
    }
    await shuffleArray(rows)
    return rows
  }

  async deleteProductById(id: number) {
    return await this.table().where("id", id).del();
  }

  async searchProductByKeyword(search_text: string) {
    let query = this.table().select("*");
    search_text.split(" ").forEach((part) => {
      if (part.length == 0) return;
      if (part.startsWith("-")) {
        part = part.replace("-", "");
        if (part.length == 0) return;
        query = query.whereNot("title", "ilike", "%" + part + "%");
      } else {
        query = query.where("title", "ilike", "%" + part + "%");
      }
    });
    query = query.limit(25);
    console.log(query.toSQL());

    return await query;
  }
}

async function shuffleArray(array: any[]) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}
