import path from 'path';
import { Page, chromium } from 'playwright'


async function main() {
    const browser = await chromium.launch();
    const context = await browser.newContext({
        userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.72 Safari/537.36'
    })
    let page: Page = await context.newPage()


    // await page.goto('https://www.parknshop.com/zh-hk/')
    await page.goto('https://www.parknshop.com/zh-hk/tissues/lc/310000')
    const title = await page.title()
    console.log(title)
    const allItemsContainer = await page.$$('.item .border .padding')

    for (let Item of allItemsContainer) {
        // const name = (await Item.textContent())?.replace(/\s/g,'')
        let name = await (await Item.$('.name'))?.textContent()
        name = name?.replace(/\s/g, '')
        console.log('Item name: ' + name)

        let price = await (await Item.$('.price-container .display-price .discount'))?.textContent()
        console.log('Item price: ' + price)

        let itemHref = await (await Item.$('.productPrimaryPic a'))?.getAttribute('href')
        console.log(path.join('https://www.parknshop.com/zh-hk/tissues/lc/310000', itemHref as string))
    }
}

main()