// import path from 'path';
import { Page, chromium } from 'playwright'

const href_txt = "https://www.ztore.com"

async function main() {
    const browser = await chromium.launch();
    const context = await browser.newContext({
        userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.72 Safari/537.36'
    })
    let page: Page = await context.newPage()


    // await page.goto('https://www.ztore.com/tc/category/group/featured-products', { waitUntil: 'networkidle' })
    await page.goto('https://www.ztore.com/tc/category/group/featured-products')

    let list = await page.evaluate(() => {
        let urlList: string[] = []
        document.querySelectorAll('img').forEach(img => {
            urlList.push(img.src)
        })
        return urlList
    })
    console.log({ list });

    let productList = await page.evaluate(() => {

        function scrollToLast(cb: () => void) {
            let es = document.querySelectorAll('.ProductItem')
            console.log('number of products:', es.length);
            let last = es[es.length - 1]
            last.scrollIntoView()
            setTimeout(() => {
                let es2 = document.querySelectorAll('.ProductItem')
                if (es2.length != es.length) {
                    return scrollToLast(cb)
                } else {
                    cb()
                }
            }, 500)
        }

        type ProductItem = { name: string, image_src: string, price: number }
        return new Promise<ProductItem[]>((resolve) => {
            scrollToLast(() => {
                let productList: Array<Promise<ProductItem | null>> = []
                document.querySelectorAll('.ProductItem').forEach(root => {
                    productList.push(new Promise<ProductItem | null>((resolve, reject) => {
                        function loadImage() {
                            let name = root.querySelector('.name[title]')?.textContent?.trim()
                            let img = root.querySelector<HTMLImageElement>('.img img')
                            let priceText = root.querySelector('.price .promotion')?.textContent || root.querySelector('.price')?.textContent
                            // console.log({ name, priceText });
                            if (!name || !img || !priceText) {
                                resolve(null)
                                return
                            }
                            let price = +priceText.replace('$', '').trim()
                            if (!price) {
                                reject(new Error('failed to parse price'))
                                return
                            }
                            let image_src = img?.src
                            if (image_src.startsWith('data:')) {
                                // console.log('image still loading?:', img);
                                img.scrollIntoView()
                                setTimeout(loadImage, 500)
                            } else {
                                resolve({ name, image_src, price })
                            }
                        }
                        loadImage()
                    }))
                })
                Promise.all(productList).then(maybeProductList => {
                    let productList = maybeProductList = maybeProductList.filter(product => product != null)
                        .map(product => product!)
                    console.log('resolve:', productList);
                    resolve(productList)
                })
            })
        })
    })
    console.log({ productList });


    console.log(await page.$$eval('img', imgs => imgs.map(img => img.getAttribute("src"))));

    // const title = await page.title()
    // console.log(title)

    const allItemsContainer = await page.$$(".list .ProductList .ProductItem")
    console.log(allItemsContainer.length);


    //console.log(await allItemsContainer[0].innerText());
    // console.log(await allItemsContainer[1].innerText());
    let itemList = []
    for (let item of allItemsContainer) {
        let itemObject = {}

        //href
        let href = await (await item.$("a"))?.getAttribute("href")
        href = href_txt + href

        //name
        let name = await (await item.$("a .name_bbd .name .TruncateBox"))?.innerText()
        if (name == "" || name == undefined) {
            continue
        }
        // name = name?.replace(/\s/g, )

        //price
        let price = await (await item.$(".price .promotion"))?.innerText()
        price = price?.replace(/\s/g, '')

        //let t = await item.$(".img div img");
        let img_alt = await (await item.$(".img div img"))?.getAttribute("alt")
        let img_src = await (await item.$(".img div img"))?.getAttribute("src")
        let img_srcset = await (await item.$(".img div img"))?.getAttribute("srcset")

        //const imgs = await item.$("[srcSet]");
        //console.log(imgs);
        // console.log(`name: ${name} price: ${price}`)
        itemObject = {
            name: name,
            price: price,
            href: href,
            img_alt: img_alt,
            img_src: img_src,
            img_srcset: img_srcset
        }
        itemList.push(itemObject)
    }
    console.log(itemList);


    // for(let Item of allItemsContainer){
    //     // const name = (await Item.textContent())?.replace(/\s/g,'')
    //     let name = await (await Item.$('.name'))?.textContent()
    //     name = name?.replace(/\s/g,'')
    //     console.log('Item name: ' + name)

    //     let price = await (await Item.$('.price-container .display-price .discount'))?.textContent()
    //     console.log('Item price: ' + price)

    //     let itemHref = await (await Item.$('.productPrimaryPic a'))?.getAttribute('href')
    //     console.log(path.join('https://www.parknshop.com/zh-hk/tissues/lc/310000',itemHref as string))
    // }

}

main()
