import path from 'path';
import * as playwright from 'playwright'
import {Top10ProductService, NewTop10Product} from '../../services/top10product.service'
import {knex} from '../../server/knex_config'

let itemsInfo:NewTop10Product[]=[];
export async function scraper_parkn() {
    let top10ProductService = new Top10ProductService(knex)
    const browser = await playwright.chromium.launch({headless:true});
    const context = await browser.newContext({
        userAgent:'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36'
    })
    let page = await context.newPage()
    let classesInfo = await getAllClasses(page, 'https://www.parknshop.com/zh-hk/')
    console.log('Parkn: Start scraping')
    // console.log(classesInfo)
    for (let title in classesInfo) {
        let category_id;
        switch (title) {
            case '餅乾、零食、糖果':
                category_id = 1
                break
            case '飲品、即沖飲品':
                category_id = 2
                break
            case '酒類':
                category_id = 3
                break
            case '米、麵、食油': case '罐頭、醬料、湯': case '早餐及果醬':
                category_id = 4
                break
            case '紙巾、廁紙': case '家居清潔':
                category_id = 5
                break
            case '寵物食品及護理':
                category_id = 6
                break
            default:
                continue
        }

        let link = classesInfo[title]
        console.log('Parkn: scraping for class', title)
        await getAllItems(page, link, category_id)
        console.log('Parkn: Finished scraping for items of',title )
        await sleep(3000)
        if(title=='家居清潔'){
            await top10ProductService.createProductBatch(itemsInfo)
        }
    }
    browser.close()
}


export async function getAllItems(page: playwright.Page, pageLink: string, category_id: number) {
    await page.goto(pageLink)
    await sleep(5000)
    const allItemsContainer = await page.$$('.item .border .padding')
    let count = 0;
    for (let i = 0; i < 100; i++) {
        if (category_id == 4 && i == 8) {
            return
        } else if (category_id == 5 && i == 12) {
            return
        }
        let Item = allItemsContainer[i]
        // const name = (await Item.textContent())?.replace(/\s/g,'')
        if (!Item) {
            continue
        }
        let title = await (await Item.$('.name'))?.textContent()
        title = title?.replace(/\s/g, '')
        // console.log('Item name: ' + title)

        let price = await (await Item.$('.price-container .display-price .discount'))?.textContent()
        price = price?.replace('HK$', '').replace(',', '')
        // console.log('Item price: ' + price)

        let href = await (await Item.$('.productPrimaryPic a'))?.getAttribute('href')
        href = path.join('https://www.parknshop.com/', href as string)
        // console.log('Item href: '+href)

        let item_id = href?.split('/').slice(-1)[0]


        let imgSrc = await (await Item.$('.productPrimaryPic a img'))?.getAttribute('data-original')
        imgSrc = path.join('https://www.parknshop.com', imgSrc as string)
        // console.log('Item img source: '+imgSrc)

        if (!title || !price || !href || !imgSrc) {
            continue
        }
        itemsInfo.push({ 'onlineshop': 'Parkn', 'title': title, 'price': +price, 'item_id': item_id, 'category_id': category_id, 'href': href, 'img_src': imgSrc })
        count++
        // console.log({ 'onlineshop': 'Parkn', 'title': title, 'price': +price, 'item_id': item_id, 'category_id': category_id, 'href': href, 'img_src': imgSrc })
        if (count == 24) {
            return
        }
    }
}

export async function getAllClasses(page: playwright.Page, pageLink: string) {
    let classesInfo = {}
    await page.goto(pageLink)
    const allClassesLabel = await page.$$('.category-title a')
    for (let classLabel of allClassesLabel) {
        let title = await (await classLabel.$('.text'))?.textContent()
        // console.log('Class title: ',title)

        let href = await classLabel.getAttribute('href')
        // console.log('Class href: ', path.join('https://www.parknshop.com',href as string))
        classesInfo[title as string] = path.join('https://www.parknshop.com', href as string)
    }
    return classesInfo
}

function timeout(ms: number | undefined) {
    return new Promise(resolve => setTimeout(resolve, ms))
}

export async function sleep(seconds: number) {
    await timeout(seconds)
}

