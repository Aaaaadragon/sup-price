import { Page } from "playwright";
import { NewProduct } from "../services/product.service";
import { Scraper } from "./scraper";

export class HKtvmallScraper extends Scraper {
  async search(keyword: string,numberOfItem:number=999) {
    let context = await this.browser.newContext({userAgent:'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36'})
    let page = await context.newPage();
    keyword = keyword.replace('/\s/g','%20')
    let url =
      "https://www.hktvmall.com/hktv/zh/search_a/?keyword=" +
      encodeURIComponent(keyword);
    console.log('HKtvmall: Scraping for',keyword,'product(s)...')

    await page.goto(url);
    let productList = await this.collectProductList(page,numberOfItem);
    return productList;
  }

  async collectProductList(page: Page,numberOfItem:number) {
    let itemContainers = await page.$$(
      "#search-result-wrapper-outer .product-brief"
    );
    let productList: NewProduct[] = [];
    // Show 10 Products
    let count = 0
    for (let item of itemContainers) {
      let name = await (await item.$(".brand-product-name"))?.textContent();
      if (!name) {
        continue;
      }
      name?.toString();
      let price = await (await item.$(".price"))?.textContent();
      price = price?.replace("$ ", "");
      if(!price) {continue}
      let img_src = await (
        await item.$(".square-wrapper img")
      )?.getAttribute("src");
      if(!img_src || img_src.length>1024) {continue}
      let href = await (await item.$("> a"))?.getAttribute("href");
      href = "https://www.hktvmall.com/hktv/zh/" + href;
      if(!href || href.length>1024){continue}
      let item_id = href?.split("/").slice(-1)[0];
      productList.push({
        onlineshop: "hktvmall",
        title: name!,
        price: +price!,
        item_id: item_id,
        href: href,
        img_src: img_src!,
      });
      count++
      if(count == numberOfItem){break}
      // console.log(className, itemsInfo)
      // console.log(productInfo.category_id)
    } //  !!Release!!
    return productList;
  }
  // console.log(itemsInfo);
}
