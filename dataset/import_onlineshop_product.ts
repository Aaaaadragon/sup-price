import fs from 'fs'
import path from 'path'
import { knex } from '../server/knex_config'
import { ProductService } from '../services/product.service'

export async function importProducts() {
    try {
        let productService = new ProductService(knex)
        let jsonString = fs.readFileSync(path.join(__dirname,'onlineshop_product_202202031656.json'),{encoding:'utf-8'})
        let dataset = JSON.parse(jsonString)
        console.log(typeof dataset)
        console.log('Number of product to be inserted: ',dataset.onlineshop_product.length)
        for(let product of dataset.onlineshop_product){
            await productService.createProduct(product)   
        }
        console.log('Product data insert finished!')
        
    } catch (error) {
        console.log(error)
    }
}

