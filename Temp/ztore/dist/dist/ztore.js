"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// import path from 'path';
const playwright_1 = require("playwright");
const href_txt = "https://www.ztore.com";
async function main() {
    var _a, _b, _c, _d, _e, _f;
    const browser = await playwright_1.chromium.launch();
    const context = await browser.newContext({
        userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.72 Safari/537.36'
    });
    let page = await context.newPage();
    // await page.goto('https://www.ztore.com/tc/category/group/featured-products', { waitUntil: 'networkidle' })
    await page.goto('https://www.ztore.com/tc/category/group/featured-products');
    let list = await page.evaluate(() => {
        let urlList = [];
        document.querySelectorAll('img').forEach(img => {
            urlList.push(img.src);
        });
        return urlList;
    });
    console.log({ list });
    let productList = await page.evaluate(() => {
        function scrollToLast(cb) {
            let es = document.querySelectorAll('.ProductItem');
            console.log('number of products:', es.length);
            let last = es[es.length - 1];
            last.scrollIntoView();
            setTimeout(() => {
                let es2 = document.querySelectorAll('.ProductItem');
                if (es2.length != es.length) {
                    return scrollToLast(cb);
                }
                else {
                    cb();
                }
            }, 500);
        }
        return new Promise((resolve) => {
            scrollToLast(() => {
                let productList = [];
                document.querySelectorAll('.ProductItem').forEach(root => {
                    var _a, _b, _c, _d, _e;
                    let name = (_b = (_a = root.querySelector('.name[title]')) === null || _a === void 0 ? void 0 : _a.textContent) === null || _b === void 0 ? void 0 : _b.trim();
                    let image_src = (_c = root.querySelector('.img img')) === null || _c === void 0 ? void 0 : _c.src;
                    let priceText = ((_d = root.querySelector('.price .promotion')) === null || _d === void 0 ? void 0 : _d.textContent) || ((_e = root.querySelector('.price')) === null || _e === void 0 ? void 0 : _e.textContent);
                    console.log({ name, image_src, priceText });
                    if (!name || !image_src || !priceText)
                        return;
                    let price = +priceText.replace('$', '').trim();
                    if (!price) {
                        throw new Error('failed to parse price');
                    }
                    productList.push({ name, image_src, price });
                });
                console.log('resolve:', productList);
                resolve(productList);
            });
        });
    });
    console.log({ productList });
    console.log(await page.$$eval('img', imgs => imgs.map(img => img.getAttribute("src"))));
    // const title = await page.title()
    // console.log(title)
    const allItemsContainer = await page.$$(".list .ProductList .ProductItem");
    console.log(allItemsContainer.length);
    //console.log(await allItemsContainer[0].innerText());
    // console.log(await allItemsContainer[1].innerText());
    let itemList = [];
    for (let item of allItemsContainer) {
        let itemObject = {};
        //href
        let href = await ((_a = (await item.$("a"))) === null || _a === void 0 ? void 0 : _a.getAttribute("href"));
        href = href_txt + href;
        //name
        let name = await ((_b = (await item.$("a .name_bbd .name .TruncateBox"))) === null || _b === void 0 ? void 0 : _b.innerText());
        if (name == "" || name == undefined) {
            continue;
        }
        // name = name?.replace(/\s/g, )
        //price
        let price = await ((_c = (await item.$(".price .promotion"))) === null || _c === void 0 ? void 0 : _c.innerText());
        price = price === null || price === void 0 ? void 0 : price.replace(/\s/g, '');
        //let t = await item.$(".img div img");
        let img_alt = await ((_d = (await item.$(".img div img"))) === null || _d === void 0 ? void 0 : _d.getAttribute("alt"));
        let img_src = await ((_e = (await item.$(".img div img"))) === null || _e === void 0 ? void 0 : _e.getAttribute("src"));
        let img_srcset = await ((_f = (await item.$(".img div img"))) === null || _f === void 0 ? void 0 : _f.getAttribute("srcset"));
        //const imgs = await item.$("[srcSet]");
        //console.log(imgs);
        // console.log(`name: ${name} price: ${price}`)
        itemObject = {
            name: name,
            price: price,
            href: href,
            img_alt: img_alt,
            img_src: img_src,
            img_srcset: img_srcset
        };
        itemList.push(itemObject);
    }
    console.log(itemList);
    // for(let Item of allItemsContainer){
    //     // const name = (await Item.textContent())?.replace(/\s/g,'')
    //     let name = await (await Item.$('.name'))?.textContent()
    //     name = name?.replace(/\s/g,'')
    //     console.log('Item name: ' + name)
    //     let price = await (await Item.$('.price-container .display-price .discount'))?.textContent()
    //     console.log('Item price: ' + price)
    //     let itemHref = await (await Item.$('.productPrimaryPic a'))?.getAttribute('href')
    //     console.log(path.join('https://www.parknshop.com/zh-hk/tissues/lc/310000',itemHref as string))
    // }
}
main();
//# sourceMappingURL=ztore.js.map