import playwright, { Browser, Page } from 'playwright'
async function main() {
    let browser = await playwright.chromium.launch({headless:false})
    const context = await browser.newContext({
        userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.72 Safari/537.36'
    })
    let page: playwright.Page = await context.newPage()

    let url = 'https://www.parknshop.com/zh-hk/snacks-desserts/nuts-seeds/c/030400'
    await page.goto(url)

    await clickShowMoreBtn(browser,page)

    
}

main()


export async function clickShowMoreBtn(browser:Browser,page:Page) {
    try {
        // let isShowMoreBtnVisible = await page.evaluate(async()=>{
        //     let showMoreBtn = document.querySelector('.showMore .button')
            
        //     return showMoreBtn ?true:false
        // })
        let isShowMoreBtnHidden = await (await page.$('.showMore .button'))?.isHidden()
        console.log('Is showmore available? answer: ',isShowMoreBtnHidden)
        if (!isShowMoreBtnHidden){
            console.log('showMore button being clicked...')
            await page.click('.showMore .button')
            clickShowMoreBtn(browser,page)
        }
    } catch (error) {
            const allItemsContainer = await page.$$('.item .border .padding')
            console.log('Number of all items: ',allItemsContainer.length)
            for (let itemInfo of allItemsContainer){
                
            }
            
            browser.close()
        // console.log('ERROR: ',error)
    }
        
        
}

