import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if(!(await knex.schema.hasTable('onlineshop_product'))){
        await knex.schema.createTable('onlineshop_product',table=>{
            table.increments('id')
            table.string('onlineshop').notNullable()
            table.string('title').notNullable()
            table.float('price').notNullable()
            table.string('item_id').notNullable()
            table.integer('category_id').references('category.id').nullable().defaultTo(0)
            table.string('href',1024).notNullable()
            table.string('img_src',1024).notNullable()
            table.boolean('is_training_data').defaultTo('false')
            table.integer('click_count').defaultTo(0)
            table.timestamps(false,true)
        })
    }
    
    if(!(await knex.schema.hasTable('top_10_items'))){
        await knex.schema.createTable('top_10_items',table=>{
            table.increments('id')
            table.string('onlineshop').notNullable()
            table.string('title').notNullable()
            table.float('price').notNullable()
            table.string('item_id').notNullable()
            table.integer('category_id').references('category.id')
            table.string('href',1024).notNullable()
            table.string('img_src',1024).notNullable()
            table.integer('click_count').defaultTo(0)
            table.timestamps(false,true)
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('onlineshop_product')
    await knex.schema.dropTableIfExists('top_10_items')
}

