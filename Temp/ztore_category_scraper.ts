// import path from 'path';
import { Page, chromium } from 'playwright'

let ztoreCategory: any = ""
export async function getCategory(url: string) {
    const browser = await chromium.launch();
    const context = await browser.newContext({
        userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.72 Safari/537.36'
    })
    let page: Page = await context.newPage()

    await page.goto(url)
    const allItemsContainer = await page.$$('.floatl__input')
    let category = await allItemsContainer[0].getAttribute("value")
    browser.close()
    ztoreCategory = category

    let category_id;
    switch (ztoreCategory) {
        case '零食':
            category_id = 1
            break
        case '飲品':
            category_id = 2
            break
        case '酒類產品':
            category_id = 3
            break
        case '糧油雜貨':
            category_id = 4
            break
        case '家居用品':
            category_id = 5
            break
        case '狗仔專區': case '貓咪專區':
            category_id = 6
            break
        default:
            break
    }

    console.log({ categoryID: category_id, category: ztoreCategory });
    // return allItemsContainer[0].getAttribute("value")
}

// let result = main().then()
// console.log(result)
let url = "https://www.ztore.com/tc/product/instant-noodle-beef-6005275#query=%E5%87%BA%E5%89%8D%E4%B8%80%E4%B8%81%20%E7%89%9B%E8%82%89"
url = "https://www.ztore.com/tc/product/coke-1000253#query=cocacola"
url =
    "https://www.ztore.com/tc/product/meow-meow-valuable-meal-set-6006664#query=%E8%B2%93%E8%B2%93%E5%84%AA%E6%83%A0%E5%A5%97%E9%A4%90"
getCategory(url)