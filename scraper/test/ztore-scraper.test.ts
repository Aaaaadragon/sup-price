import { chromium } from "playwright";
import { ProductService } from "../../services/product.service";
import { knex } from "../../server/knex_config";
import { Top10ProductService } from "../../services/top10product.service";
import { ZtoreScraper } from "../ztore-scraper";
// import fs from "fs";
// import { strReplaceAll, extract_lines } from "@beenotung/tslib/string";

async function main(keyword: string) {
  let productService = new ProductService(knex);
  let top10ProductService = new Top10ProductService(knex);
  // let browser = await chromium.launch({ headless: false });
  let browser = await chromium.launch();
  //   let browser = null as any;
  let scrapper = new ZtoreScraper(
    knex,
    browser,
    productService,
    top10ProductService
  );

  let list = await scrapper.searchAndStore(keyword); // 1.維多卡酒莊 2.氣泡酒

  console.log("found", list.productList.length, "results");

  //   let html = fs.readFileSync("output.html").toString();
  //   let category_id = scrapper.parseProductCategory(html);
  //   console.log({ category_id });

  browser.close();
  // knex.destroy();
}

let keywordArray = 
[
  ''
];


for (let i = 0; i < keywordArray.length; i++) {
  try{

    setTimeout(() => {
      main(keywordArray[i]);
    }, 1000 * 15 * i);
  }catch (err) {
    console.log(`error: ${keywordArray[i]}`);
    
  }
}





export let wordList = `

"威士忌",
  "貓糧",
  "小食",
  "牛肉",
  "高樂",
  "重量",
  "膚抑",
  "印花",
  "白玫瑰",
  "花香",
  "韌純",
  "輕巧",
  "增量",
  "香米",
  "糯米",
  "麻油",
  "花生",
  "南瓜",
  "北海道",
  "咖啡",
  "天然",
  "洗衣",
  "消毒",
  "牛奶",
  "衣物",
  "芝士",
  "曲奇",
  "迷你",
  "橄欖",
  "即食",
  "意大利",
  "粟米",
  "忌廉",
  "果汁",
  "日本",
  "配方",
  "清新",
  "果仁",
  "泉水",
  "蕃茄",
  "抗菌",
  "白酒",
  "牛油",
  "薄荷",
  "鮑魚",
  "蜂蜜",
  "蛋糕",
  "花生油",
  "提子",
  "零食",
  "紫菜",
  "添加",
  "蔬菜",
  "手套",
  "高鈣",
  "日式",
  "口味",
  "蒟蒻",
  "橡皮",
  "低脂",
  "早餐",
  '果味',
  '拉麵',
  '橡皮糖',
  '葡萄',
  '蜜糖',
  '胡椒',
  '可口',
  '穀物',
  '健康',
  '咖喱',
  '香草',
  '萬用',
  '午餐',
  '奶茶',
  '汽水',
  '辣椒',
  '美國',
  '加糖',
  '玫瑰',
  '海道',
  '午餐肉',
  '麻辣',
  '低糖',
  '高級',
  '多用',
  '火腿',
  '香味',
  '混合',
  '多用途',
  '頂級',
  '用途',
  '地板',
  '家庭',
  '椰子',
  '芒果',
  '米粉',
  '酒精',
  '薰衣草',
  '啫喱',
  '莎當妮',
  '榛子',
  '當妮',
  '焦糖',
  '中國',
  '可可',
  '單包',
  '玉米',


  '乾糧',
'蜜桃',
'草莓',
'英國',
'漂白',
'萬潔',
'黑芝麻',
'生抽',
'無麩',
'家居',
'雀巢',
'味道',
'豆腐',
'腰果',
'通心粉',
'一款',
'糖果',
'韓國',
'蘑菇',
'金牌',
'能量',
'菠蘿',
'海洋',
'糙米',
'白朱',
'玩具',
'香花',
'鹽焗',
'薄脆',
'巧克力',
'水果',
'柚子',
'薄荷糖',
'棉花',
'羊肉',
'貓砂',
'高竇貓',
'脆脆',
'香脆',
'粒粒',
'芥末',
'蛋白',
'大豆',
'番茄',
'乾條',
'蘇打',
'麵粉',
'波浪',
'手製',
'甜品',
'乳酪',
'棉花糖',
'奶粉',
'橙汁',
'打水',
'拖把',
'優質',
'蝦條',
'甜酒',
'抹布',
'曲奇餅',
'黑糖',
'普洱',
'垃圾',
'無酒精',
'蘆薈',
'發送',
'咖哩',
'消化',
'香橙',
'菊花',
'蘇維',
'砂糖',








`


export let failList=
`
洗衣粉
月桂
桂冠
干邑
至尊
"新年",
"茉莉花",
"白米",
"慈康",
"高原",
"菊花茶",
"馬爹"
`
export let okList=
`
衛生紙
//"餅乾"
//"朱古力"

//"果仁"
//"糖果"

//"薯片"

//"蛋卷"
//"蛋糕"

//"肉乾"
//"花生"

//"紫菜"

//"乾果"
//"脆片"

//"甜品"
//"啫喱"

朱古力
薯片
啤酒
杏仁
茉莉
芝麻
檸檬
清香
優惠
系列
"辣味"
花茶
"豆奶",
`