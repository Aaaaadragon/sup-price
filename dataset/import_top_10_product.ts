import fs from 'fs'
import path from 'path'
import { knex } from '../server/knex_config'
import { Top10ProductService } from '../services/top10product.service'

export async function importTopTen() {
    try {
        let top10ProductService = new Top10ProductService(knex)
        let jsonString = fs.readFileSync(path.join(__dirname,'top_10_items_202202031715.json'),{encoding:'utf-8'})
        let dataset = JSON.parse(jsonString)
        // console.log(typeof dataset)
        console.log('Number of product to be inserted: ',dataset.top_10_items.length)
        await top10ProductService.createProductBatch(dataset.top_10_items)
        console.log('Product data insert finished!')
        
    } catch (error) {
        console.log(error)
    }
}

