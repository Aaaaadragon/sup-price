import {importProducts} from './import_onlineshop_product'
import { importSearchHistory } from './import_text_searching_history'
import { importTopTen } from './import_top_10_product'
async function getAllBasicData() {
    await importSearchHistory()
    await importTopTen()
    await importProducts()
}

getAllBasicData()