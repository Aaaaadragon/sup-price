import fs from "fs";
import path from "path";
import fetch from "node-fetch";
import playwright from "playwright";
// import { ProductService } from "../services/product.service";

let sleep = async function (time: number) {
  await timeout(time);
};

async function timeout(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

// ProductService;
async function imgDownloader() {
  // let productService = new ProductService(knex);
  let browser = await playwright.chromium.launch({ headless: false });
  const context = await browser.newContext({
    userAgent:
      "Mozilla / 5.0(Windows NT 10.0; Win64; x64; rv: 96.0) Gecko/20100101 Firefox/96.0",
  });
  let page: playwright.Page = await context.newPage();

  //    Page = snacks
  await page.goto(
    "https://www.hktvmall.com/hktv/zh/search_a?keyword=%E9%9B%B6%E9%A3%9F&bannerCategory=AA11150000000&category=AA11150000000"
  );

  //    wait the selector load
  await page.waitForSelector(".searchTree__cat.searchTree__cat--leaf");
  let catNames = await page.evaluate(() => {
    let catNames: string[] = [];
    document
      .querySelectorAll(
        ".searchTree__cat.searchTree__cat--leaf .searchTree__cat__name"
      )
      .forEach((e) => {
        catNames.push(e.textContent?.trim() || "");
      });
    return catNames.filter((s) => s);
  });
  console.log("catNames:", catNames);

  //    Loop out all class of snacks
  for (let catName of catNames) {
    //  clear product list before click category
    await page.evaluate(() => {
      let list = document.querySelector(".product-brief-list");
      if (list) {
        list.innerHTML = "";
      }
    });

    //  Create directory
    fs.mkdirSync(
      path.join(`/Users/Ren/Project2/sup-price/AI_dataset/${catName}`),
      {
        recursive: true,
      }
    );
    console.log("create a directory");

    //  click category
    console.log("click category:", catName);
    await page.evaluate(
      ({ catName }) =>
        new Promise<void>((resolve) => {
          let found = false;
          function loop() {
            document
              .querySelectorAll<HTMLSpanElement>(
                ".searchTree .searchTree__cat__name"
              )
              .forEach((e) => {
                if (e.textContent?.trim() == catName) {
                  e.click();
                  found = true;
                  resolve();
                }
              });
            if (!found) {
              setTimeout(loop, 33);
            }
          }
          loop();
        }),
      { catName }
    );

    // wait for product list
    let start = Date.now();
    console.log("start wait");
    await page.waitForSelector(".product-brief-list .product-brief-wrapper");
    let end = Date.now();
    console.log("finish wait", end - start);
    //  Loop out product
    let productList = await page.$$(".product-brief-wrapper");
    console.log("Length of productList:", productList.length);
    let count = 0;
    let skipCount = 0;
    for (let product of productList) {
      let productName = await (
        await product.$(".brand-product-name")
      )?.textContent();

      //    ignore ads
      if (await product.$(".Product-surface p")) {
        skipCount++;
        continue;
      }
      let image_src = await (
        await product.$(".image-container img")
      )?.getAttribute("src");
      console.log("Name of product:", productName);
      console.log("image link:", image_src);

      if (
        fs.existsSync(
          `C:/Users/Ren/Project2/sup-price/AI_dataset/${catName}/${productName}.jpg`
        )
      ) {
        console.log(`${productName}.jpg is exist`);
        count++;
        continue;
      }
      //  Image Resize
      let resizedImgSrc =
        "https://images.weserv.nl/?w=160&h=160&url=" + image_src;
      await download(resizedImgSrc, productName!, catName!);
      count++;
    }

    // go back to main category
    console.log("wait 2 seconds");
    await sleep(2000);
    console.log("Go outside");

    // await page.goBack();
    await page.evaluate(() => {
      let es = document.querySelectorAll<HTMLSpanElement>(
        ".searchTree__cat.searchTree__cat--back"
      );
      let e = es[1];
      if (!e) {
        throw new Error("failed to find main category back button");
      }
      e.click();
    });
    console.log("Total count download:", count);
    console.log("Total skip product", skipCount);
  }
}
//   let tree = await page.$$(".searchTree .searchTree__cat--leaf");
//   for (let item of tree) {
//     let itemSpan = await item.$(".searchTree__cat__name");
//     if (!itemSpan) {
//       console.error("failed to find item span");
//       throw new Error("unexpected dom");
//     }
//     let itemName = await itemSpan.textContent();
//     console.log("For loop Item:", itemName);

//     // await page.locator(`.searchTree__cat__name:text("${span}")`).click();
//     await itemSpan.click();
//     console.log("clicked on:", itemName);
//     // console.log("Get inside and waiting product");

//  Photo Download
async function download(ImgSrc: string, imgTitle: string, itemName: string) {
  const response = await fetch(ImgSrc);
  const buffer = await response.buffer();
  //   const arrayBufferView = new Uint16Array(buffer)
  fs.writeFile(
    `C:/Users/Ren/Project2/sup-price/AI_dataset/${itemName}/${imgTitle}.jpg`,
    buffer,
    () => console.log("finished downloading!")
  );
}

//  Auto turn off when throwing ERROR
imgDownloader().catch((e) => {
  console.error(e);
  process.exit(1);
});
