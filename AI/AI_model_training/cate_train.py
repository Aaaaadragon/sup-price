#%% import library
from cProfile import label
from email.mime import base
import os
from pickletools import optimize
from sre_parse import CATEGORIES
from xml.etree.ElementInclude import include
import cv2
import tensorflow as tf
import numpy as np
import random
import matplotlib.pyplot as plt
# %% load label list
train_dir = '../AI_dataset/Product_category_dataset/train'
all_categories = os.listdir(train_dir)
all_categories.remove('.DS_Store')
print('Total categories count:', len(all_categories))
# %% quick scan dataset
skip_categories = []
nonskip_categories = []
category_counts = []
min_count = 100
max_count = 200
for category in all_categories:
    files = os.listdir(os.path.join(train_dir,category))
    count = len(files)
    if count > max_count:
        count = max_count
    category_counts.append(count)
    print(category, count)
    if count < min_count:
        skip_categories.append(category)
    else:
        nonskip_categories.append(category)
plt.bar(
    x=list(range(len(all_categories))),
    height=category_counts
)
plt.show()
print('nonskip categories count:',len(nonskip_categories))
print('nonskip categories:', nonskip_categories)
print('all categories count: ', category_counts)
# %% config image size
image_width = 160
image_height = 160
image_size = (image_width,image_height)
# %% load dataset
all_images = []
all_categories = []
category_counts = []
categories = nonskip_categories
for idx,category in enumerate(categories):
    print('scan label:', category)
    files = os.listdir(os.path.join(train_dir,category))
    count = len(files)
    category_counts.append(count)
    i = 0
    for file in files:
        if i > max_count:
            break
        if not file.endswith('.jpg'):
            continue
        i += 1
        file = os.path.join(train_dir,category,file)
        try:
            img = cv2.imread(file)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            img = cv2.resize(img,image_size)
            img = tf.convert_to_tensor(img, dtype=tf.float32)
            img = (img/127.5) - 1
            all_images.append(img)
            all_categories.append(idx)
        except:
            print('failed to load image:', file)
            os.unlink(file)
print('categories to be trained:',categories)
plt.bar(
    x=list(range(len(categories))),
    height=category_counts
)
plt.show()
# %% 
X = np.array(all_images, dtype=np.float32)
Y = np.array(all_categories).reshape(len(X),1)
print('X shape:', X.shape)
print('Y shape:', Y.shape)
# %% config base_model 
base_model = tf.keras.applications.MobileNetV2(
    input_shape=(image_width, image_height, 3),
    include_top=False,
    weights='imagenet',
    pooling='avg'
)

base_model.trainable = False
base_model.summary()
# %% create model with base_model
model = tf.keras.Sequential()
model.add(base_model)
model.add(tf.keras.layers.BatchNormalization())
model.add(tf.keras.layers.Dense(len(categories)))
model.add(tf.keras.layers.Softmax())
model.summary()
# %%
train_leaning_rate = 0.005
model.compile(
    optimizer=tf.keras.optimizers.RMSprop(lr=train_leaning_rate),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=['accuracy']
)
# %% split validation samples
train_X = []
train_Y = []
test_X = []
test_Y = []
validation_split = 0.1
for i in range(len(X)):
    if random.random() < validation_split:
        test_X.append(X[i])
        test_Y.append(Y[i])
    else:
        train_X.append(X[i])
        train_Y.append(Y[i])
train_X = np.array(train_X)
train_Y = np.array(train_Y)
test_X = np.array(test_X)
test_Y = np.array(test_Y)
print('train:', train_X.shape)
print('test:', test_X.shape)
# %% fitting model
history = model.fit(
    epochs=5,
    x=train_X,
    y=train_Y,
    validation_data=(test_X, test_Y),
    # validation_split=0.1,
    shuffle=True,
    batch_size=20,
    use_multiprocessing=True,
    workers=8
)
# %%
model.save('../Python_AI_server/recognition_category_model',overwrite=True);
# %%
