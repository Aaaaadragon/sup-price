//Need to download a specified version of node-fetch by the following command line:
//npm install node-fetch@^2.6.1 npm install --save-dev @types/node-fetch@2.5.12
import fs from "fs"
import fetch from 'node-fetch'
import {knex} from '../server/knex_config'
import { ProductService } from "../services/product.service";

async function main() {
    let productService = new ProductService(knex)
    const itemsInfo = await productService.getAllTrainingData()
    console.log(itemsInfo.length)
    // let imgSrcList = []
    for(let iteminfo of itemsInfo){
        console.log('Title: ',iteminfo.title)
        console.log('Img source: ', iteminfo.img_src)
        let fixedImgSrc = iteminfo.img_src
        fixedImgSrc = fixedImgSrc.slice(0,7) + '/' + fixedImgSrc.slice(7)
        let resizedImgSrc = 'https://images.weserv.nl/?w=160&h=160&url=' + fixedImgSrc
        console.log('Resized img source: ',resizedImgSrc)
        // imgSrcList.push([iteminfo.title,iteminfo.img_src])
        await download(resizedImgSrc,iteminfo.title)
    }

    
}


main()



async function download(ImgSrc:string,imgTitle:string) {
  const response = await fetch(ImgSrc);
  const buffer = await response.buffer();
//   const arrayBufferView = new Uint16Array(buffer)
  fs.writeFile(`/Users/aaadragon/Desktop/sup-price/AI_Dataset/train/${imgTitle}.jpg`, buffer, () => 
    console.log('finished downloading!'));
}
