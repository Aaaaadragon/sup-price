// const fs = require('fs');
import fs from "fs"
// const client = require('https');
import client from "https"

function downloadImage(url: string, filepath: string) {
    return new Promise((resolve, reject) => {
        client.get(url, (res: any) => {
            if (res.statusCode === 200) {
                // console.log('content-type:', res.headers['content-type']);
                // console.log('content-type:', res.headers['content-type'].split("/")[1]);
                // if (res.headers['content-type'].split("/")[1] == "jpeg") {
                //     filepath = filepath + ".jpg"
                // } else if (res.headers['content-type'].split("/")[1] == "png") {
                //     filepath = filepath + ".png"
                // }
                filepath = filepath + "." + res.headers['content-type'].split("/")[1]
                // console.log(filepath);

                // console.log('content-length:', res.headers['content-length']);
                res.pipe(fs.createWriteStream(filepath))
                    .on('error', reject)
                    .once('close', () => resolve(filepath));
            } else {
                // Consume response data to free up memory
                res.resume();
                reject(new Error(`Request Failed With a Status Code: ${res.statusCode}`));

            }
        });
    });
}

let url =
    [
        "https://www.ztore.com/_next/image?url=https%3A%2F%2Fimage.ztore.com%2Fimages%2Fztore%2Fproduction%2Fproduct%2F368px%2F1012261_1.jpg%3F1577104744&w=1920&q=75",
        "https://www.ztore.com/_next/image?url=https%3A%2F%2Fimage.ztore.com%2Fimages%2Fztore%2Fproduction%2Fproduct%2F368px%2F8001809_1.jpg%3F1639629867&w=1920&q=75",
        'https://www.google.com/images/srpr/logo3w.png'
    ]
// let path = ["./Temp/ztore/test1", "./Temp/ztore/test2", "./Temp/ztore/test3"]
let pathFolder = "./Temp/ztore/"
let filename = "test"
for (let i = 0; i < url.length; i++) {
    let path = pathFolder + filename + i
    downloadImage(url[i], path)
        .then(console.log)
        .catch(console.error);
}