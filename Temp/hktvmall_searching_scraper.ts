import { chromium } from "playwright";
import { HKtvmallScraper } from "../scraper/hktvmall-scraper";
import { knex } from "../server/knex_config";
import { ProductService } from "../services/product.service";
import { Top10ProductService } from "../services/top10product.service";
import fs from "fs";
import path from "path";


async function main() {
  let browser = await chromium.launch({ headless: true });
  let top10ProductService = new Top10ProductService(knex);
  let productService = new ProductService(knex);
  let hktvmallScraper = new HKtvmallScraper(
    knex,
    browser,
    productService,
    top10ProductService
  );
  let words = fs.readFileSync(path.join(__dirname,"wordList.txt"), { encoding: "utf-8" });
  let wordList: string[] = words.split("\n");
  let doneWords = fs.readFileSync(path.join(__dirname,"finished_download_word.txt"), {
    encoding: "utf-8",
  });
  let donwWordList: string[] = doneWords.split("\n");


  for(let word of wordList){
      try {
          
          if (donwWordList.includes(word)){continue}
          let productList = await hktvmallScraper.search(word);
          console.log("Number of products to be found:", productList.length);
          await productService.createProductBatch(productList)
          fs.writeFileSync(path.join(__dirname,"finished_download_word.txt"),word+'\n',{flag:'a+'})
          await sleep(7000)
      } catch (error) {
          console.log('ERROR:',error)
      }
  }
}

main();

async function timeout(ms:number) {
    return new Promise((resolve) => setTimeout(resolve,ms))
}
async function sleep(time:number) {
    await timeout(time)
}

