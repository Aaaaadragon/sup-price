import fs from 'fs'
import path from 'path'
import fetch from 'node-fetch'
import { knex } from '../../server/knex_config'
import { ProductService } from '../../services/product.service'

async function main() {
    let productService = new ProductService(knex)
    
    let words = fs.readFileSync('/Users/aaadragon/Desktop/sup-price/AI_data_scraper/keywords_photo_scraper/wordList.txt',{encoding:'utf-8'})
    // console.log(wordList.split('\n'))
    let wordList:string[] = words.split('\n')
    let doneWords = fs.readFileSync('/Users/aaadragon/Desktop/sup-price/AI_data_scraper/keywords_photo_scraper/finished_download_word.txt',{encoding:'utf-8'})
    let doneWordList = doneWords.split('\n')
    console.log({doneWordList})
    
    for (let word of wordList){
        // console.log({[word]:productList.length})
        let savePath = path.join('/Users/aaadragon/Desktop/sup-price/AI_dataset/Product_label_dataset',word)
        fs.mkdirSync(savePath,{recursive:true})
        let productList = await productService.searchProductByKeyword(word)
        
        if(doneWordList.includes(word)){
            console.log({word},'has been downloaded.')
            continue
        }
        for( let product of productList){
        
            let imgSrc = product.img_src
            imgSrc = imgSrc.slice(0,7) + '/' + imgSrc.slice(7)
            let resizedImgSrc = 'https://images.weserv.nl/?w=160&h=160&url=' + imgSrc
            let imgTitle = product.title
            let imgPath = path.join(savePath,imgTitle+'.jpg')
            if(fs.existsSync(imgPath)){
                console.log({imgTitle},'already been downloaded.')
                continue
            }
            console.log('Downloading image...',{imgTitle,imgPath})
            await downloadImg(resizedImgSrc,imgTitle,imgPath)
            
        }
        fs.writeFileSync('/Users/aaadragon/Desktop/sup-price/AI_data_scraper/keywords_photo_scraper/finished_download_word.txt',word+'\n',{flag:'a+'})
        
    }
}


main()

export async function downloadImg(imgSrc:string,imgTitle:string,imgPath:string) {
    const response = await fetch(imgSrc);
    console.log('Fetching from',imgSrc)
    const buffer = await response.buffer();
  //   const arrayBufferView = new Uint16Array(buffer)
    fs.writeFile(imgPath, buffer, () => 
      console.log(imgTitle+'.jpg','finished downloading!'));
  }