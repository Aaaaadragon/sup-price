import { Knex } from "knex";
import { HttpError } from "../http-error";

export type NewProduct = {
  onlineshop: string;
  title: string;
  price: number;
  item_id: string;
  category_id?: number;
  href: string;
  img_src: string;
  is_training_data?: boolean;
};

export class ProductService {
  constructor(private knex: Knex) {}

  table() {
    return this.knex(`onlineshop_product`);
  }

  async createProduct(row: NewProduct) {
    const onlineshop = row.onlineshop;
    const item_id = row.item_id;
    let result = await this.table().select("id").where({ onlineshop, item_id });
    if (result[0]) {
      console.log("Item_id:", row.item_id, "already exist.\ntry to update...");
      this.table().where('item_id','=',row.item_id).update(row)
      console.log("PROCESS <createProduct> finished")
      return;
    }
    // console.log("createProduct:", row);
    let rows: { id: number }[] = await this.table().insert(row).returning("id");
    console.log("PROCESS <createProduct> finished")
    return rows[0];
  }
  async createProductBatch(rows: NewProduct[]) {
    // console.log("Total item number", rows.length);
    let updateItemList: NewProduct[] = [];
    for (let item of rows) {
      let result = await this.table()
        .select("id")
        .where("item_id", item.item_id);

      // console.log('database select same item_id result:',result)

      if (result[0]) {
        // console.log("Item_id:", item.item_id, "already exist.\ntry to update...");
        this.table().where('item_id','=',item.item_id).update(item)
        updateItemList.push(item);
      }
    }

    rows = rows.filter((item) => !updateItemList.includes(item));
    if (rows[0] == undefined) {
      console.log("All of the results product has been created before...\ntry to update info of products...");
      for(let item of updateItemList){
        this.table().where('item_id','=',item.item_id).update(item)
      }
      console.log("Item to be updated:", updateItemList.map((item)=>item.title))
      console.log("PROCESS <createProductBatch> finished")
      return [];
    }
    
    console.log("Number of new/updated Products to be inserted:", rows.length);
    console.log("Item to be updated:", updateItemList.map((item)=>item.title))
    
    console.log("Selection finished, inserting to database...");
    let resultRows: { id: number }[] = await this.table()
    .insert(rows)
    .returning("id");
    
    console.log("PROCESS <createProductBatch> finished")
    return resultRows.map((row) => row.id);
  }

  async deleteProductById(id: number) {
    return await this.table().where("id", id).del();
  }

  async getProductByCategory(category: number) {
    let row = await this.table()
      .select("*")
      .where({
        category,
      })
      .first();
    if (!row) {
      throw new HttpError(404, "product not found");
    }
    return row;
  }

  async searchProductByKeyword(search_text: String) {
    let query = this.table().select("*");
    search_text.split(" ").forEach((part) => {
      if (part.length == 0) return;
      if (part.startsWith("-")) {
        part = part.replace("-", "");
        if (part.length == 0) return;
        query = query.whereNot("title", "ilike", "%" + part + "%");
      } else {
        query = query.where("title", "ilike", "%" + part + "%");
      }
    });
    // query = query.limit(10);
    console.log(query.toSQL());

    let rows: NewProduct[] = await query;
    return rows;
  }
  async getAllTrainingData() {
    let rows = await this.table()
      .select(["title", "img_src"])
      .where({ is_training_data: true });
    return rows;
  }
}

function timeout(ms: number | undefined) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export async function sleep(seconds: number) {
  await timeout(seconds);
}
