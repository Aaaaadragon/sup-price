import { Page } from "playwright";
import { NewProduct } from "../services/product.service";
import { Scraper } from "./scraper";
import path from "path";

export class ParknScraper extends Scraper {
  async search(keyword: string,numberOfItem:number=999){
    let context = await this.browser.newContext({userAgent:'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36'})
    let page = await context.newPage();
    let url =
      "https://www.parknshop.com/zh-hk/search?code=ALL&text=" +
      encodeURIComponent(keyword);
    console.log('Parkn: Scraping for',keyword,'product(s)...')
    await page.goto(url);
    let productList = await this.collectProductList(page,numberOfItem);
    // console.log(productList)
    return productList;
  }

  async collectProductList(page: Page, numberOfItem:number=999,train_data:boolean=false) {
      if(numberOfItem > 30){
        await this.clickShowMoreBtn(page)
      }

      const productList = await this.getAllProduct(page,numberOfItem,train_data)
        // console.log('ERROR: ',error)
        // console.log(productList)
      return productList
  }

  async clickShowMoreBtn(page:Page){
    try {
      let isShowMoreBtnHidden = await (
        await page.$(".showMore .button")
      )?.isHidden().finally();
      console.log("Is showmore Hidden? answer: ", isShowMoreBtnHidden);
  
      if (!isShowMoreBtnHidden) {
        console.log("showMore button being clicked...");
        await page.click(".showMore .button");
        await this.clickShowMoreBtn(page);
      }
    } catch (error) {
      console.log('Stop clicking showMore button, collecting items...')
      return
    }
      
  }

  async getAllProduct(page:Page,numberOfItem:number,train_data:boolean=false){
    const allItemsContainer = await page.$$(".item .border .padding");
    console.log("Number of all items: ", allItemsContainer.length);
    let productList: NewProduct[] = [];
    let count = 0;
    for (let itemInfo of allItemsContainer) {
      let title = await (await itemInfo.$(".name"))?.textContent();
      title = title?.replace(/\s/g, "");
      // console.log('itemInfo name: ' + title)

      let price = await (
        await itemInfo.$(".price-container .display-price .discount")
      )?.textContent();
      price = price?.replace("HK$", "").replace(",", "");
      // console.log('itemInfo price: ' + price)

      let href = await (
        await itemInfo.$(".productPrimaryPic a")
      )?.getAttribute("href");
      href = path.join("https://www.parknshop.com/", href as string);
      // console.log('itemInfo href: '+href)

      let item_id = href?.split("/").slice(-1)[0];

      let imgSrc = await (
        await itemInfo.$(".productPrimaryPic a img")
      )?.getAttribute("data-original");
      imgSrc = path.join("https://www.parknshop.com", imgSrc as string);
      // console.log('itemInfo img source: '+imgSrc)

      if (!title || !price || !href || !imgSrc) {
        continue;
      }
      // console.log({ 'onlineshop': 'Parkn', 'title': title, 'price': +price, 'item_id': item_id, 'href': href, 'img_src': imgSrc ,'is_training_data':true})
      productList.push({
        onlineshop: "Parkn",
        title: title,
        price: +price,
        item_id: item_id,
        href: href,
        img_src: imgSrc,
        is_training_data: train_data,
      });
      count++
      if(count == numberOfItem){
        break
      }
    }
    return productList;
  }

}

