import {Request, Response} from "express"
import { upload } from "../server/upload";
// import fetch  from "node-fetch";

import { RestController } from "./rest.controller";
import { HttpError } from "../http-error";
import { PredictService } from "../services/predict.service";

export class PredictController extends RestController {
    constructor(private predictService: PredictService){
        super();
        this.routes.post("/predict/photo",upload.single('predict_photo'), this.handleRequest(this.predict));
    }

    predict = async (req:Request, res:Response) =>{
        let photo_name = req.file?.filename
        console.log({photo_name})

        
        if(!photo_name){
            throw new HttpError(401,"請上傳圖片以作識別")
        }
        console.log({photo_name})
        console.log("Throw photo to predict...")
        return this.predictService.predict(photo_name.slice(0,-4))
        
    }
}

