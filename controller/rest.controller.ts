import { Router } from "express";
import { Request, Response } from "express";
import { HttpError } from "../http-error";
import { knex } from "../server/knex_config";

export class RestController {
  routes = Router();

  handleRequest(fn: (req: Request, res: Response) => Promise<any>) {
    return async (req: Request, res: Response) => {
      try {
        let json = await fn(req, res);
        if(req.session.session_id == 0 || req.session.session_id == undefined){
          let session_id_record = await knex('text_searching_history').max('session_id as session_id').first()
          if(req.session.session_id != session_id_record?.session_id){
            req.session.session_id = session_id_record?.session_id + 1
          }
          req.session.save()
        }
        res.json(json);
      } catch (error: any) {
        console.error("Request Fail:", {
          method: req.method,
          url: req.url,
          error,
        });
        if (error instanceof HttpError) {
          res.status(error.status).json({ error: error.message });
        } else {
          res.status(500).json({ error: String(error) });
        }
      }
    };
  }
}
