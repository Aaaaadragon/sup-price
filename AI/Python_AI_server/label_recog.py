
import tensorflow as tf
import cv2
import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

cate_recog_model = tf.saved_model.load((os.getcwd()+'/recognition_label_model'))

def load_data_as_arr(filePath):
    
    
    print(f'[INFO] processing {filePath}')
    img = cv2.imread(filePath)
    # print('Before processing to RGB:',img)
    # print('Before processing to RGB.shape', np.array(img).shape)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    # print('BGRToRGB : ',img)
    # print('BGRToRGB.shape:', np.array(img).shape)
    img = cv2.resize(img, (160,160))
    # print('After resizing.shape:', np.array(img).shape)
    img = tf.cast(img, tf.float32)
    # print('To float32: ',img)
    img = (img/127.5)-1

    return img

    



def predict_label(path:str):
    print(path)
    if path.split('\\')[-1][-4:] != '.jpg':
      path += ".jpg"
      
    predict_img = load_data_as_arr(path)
    predict_result = cate_recog_model([predict_img],training=False)
    categories = ['肉鬆', '意大利', '果汁', '酒精', '夾心餡餅', '朱古力', '泡芙', '合味道', '花生', '果仁', '薄荷糖', '白酒', '奶茶', '齋燒鵝', '糖果', '洗衣', '餅乾', '脆豆', '消毒', '脆片', '凍乾水果', '米通 米餅', '咖啡', '汽水', '手套', '穀物棒 能量棒', '威士忌', '曲奇餅', '地板', '牛奶', '麵類小食', '啫喱', '紫菜', '肉乾', '粟米片 粟米條', '啤酒', '牛奶朱古力', '豆奶', '穀物', '韓國', '小食', '蜜糖', '蛋糕', '高鈣', '貓糧', '寵物乾糧', '威化餅', '多用途', '日本', '衛生紙', '爆谷', '果乾', '珍寶珠 波板糖', '家居清潔', '布甸', '蛋卷', '乾果', '粟米', '蜂蜜', '蒟蒻', '米粉', '提子', '薯片', '忌廉', '咖喱', '胡椒', '口香糖', '和菓子', '魚乾 魚條 魚肉腸', '爆炸糖', '麵', '蝦片 蝦條 薯片', '泉水', '魷魚絲']
    result_list = []
    top_3_result = tf.nn.top_k(predict_result[0],k=3).indices.numpy()
    top_3_result = top_3_result.tolist()
    result_in_percentage = tf.nn.softmax(predict_result[0])
    rank_1_percentage = result_in_percentage[top_3_result[0]].numpy().tolist()*100
    rank_2_percentage = result_in_percentage[top_3_result[1]].numpy().tolist()*100
    rank_3_percentage = result_in_percentage[top_3_result[2]].numpy().tolist()*100
    result_list.append([categories[top_3_result[0]],rank_1_percentage])
    result_list.append([categories[top_3_result[1]],rank_2_percentage])
    result_list.append([categories[top_3_result[2]],rank_3_percentage])
    for idx,result in enumerate(result_list):
        print(f'No. {idx+1} result: {result[0]}, matching percentage: {format(result[1],".2f")}%')
    return result_list
      
