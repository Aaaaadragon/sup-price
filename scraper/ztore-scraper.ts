import { Page } from "playwright";
import { NewProduct } from "../services/product.service";
import { Scraper } from "./scraper";
import fetch from "node-fetch";
// import fs from "fs";

type ProductItem = {
  name: string;
  image_src: string;
  price: number;
  href: string;
};

let category_to_id = {
  零食: 1,
  飲品: 2,
  酒類產品: 3,
  糧油雜貨: 4,
  家居用品: 5,
  狗仔專區: 6,
  貓咪專區: 6,
};

let searchURL = "https://www.ztore.com/tc/search/";
// export let snacksURL="https://www.ztore.com/tc/category/all/snacks/"

export class ZtoreScraper extends Scraper {
  async search(keyword: string, numberOfItem: number = 48) {
    // let context = await this.browser.newContext({userAgent:'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36'})
    let page = await this.browser.newPage();
    keyword = keyword.replace('/\s/g','%20')
    console.log('Ztore: Scraping for',keyword,'product(s)...')
    let url = searchURL + encodeURIComponent(keyword);
    console.log('Ztore goto url',url)
    await page.goto(url);
    // await page.click(".viewAllButton span"); ////
    let productList = await this.collectProductList(page, numberOfItem);
    if (!"dev") {
      console.log("len:", productList.length);
      process.exit(1);
    }
    let newProductList: NewProduct[] = [];
    let count = 0;
    for (let item of productList) {
      let category_id = await this.collectProductCategory(item.href);
      // let category_id = 0
      if (!category_id) continue;
      let item_id = item.href.split("-").slice(-1)[0].split("#")[0].trim();
      if (!item_id) continue;
      newProductList.push({
        onlineshop: "ztore",
        // title: keyword + "-" + item.name,
        title: item.name,
        price: item.price,
        item_id,
        category_id: category_id,
        href: item.href,
        img_src: item.image_src,
        is_training_data: false,
      });
      count++;
      if (count == numberOfItem) {
        break;
      }
      console.log("search:", { keyword, progress: newProductList.length });
    }
    return newProductList;
  }

  async collectProductCategory(href: string): Promise<number | undefined> {
    let res = await fetch(href);
    let html = await res.text();
    return this.parseProductCategory(html);
  }

  parseProductCategory(html: string): number | undefined {
    html = html.toLowerCase();

    let index = html.indexOf("<body");
    if (index == -1) return;
    html = html.slice(index);

    let classIndex = html.indexOf("floatl__input");
    if (classIndex == -1) return;
    let before = html.slice(0, classIndex);

    index = before.lastIndexOf("input");
    if (index == -1) return;

    html = html.slice(index);

    index = html.indexOf("<");
    if (index !== -1) {
      html = html.slice(0, index);
    }

    let match = html.match(/value="(.*?)"/);
    if (!match) return;
    let category = match[1];

    return category_to_id[category];
  }

  async collectProductList(page: Page, numberOfItem: number) {
    let productList = await page.evaluate(() => {
      return new Promise<ProductItem[]>((resolve, reject) => {
        function loadImage(done: () => void) {
          let es = document.querySelectorAll(".ProductItem");
          for (let root of Array.from(es)) {
            let img = root.querySelector<HTMLImageElement>(".img img");
            if (img?.src.startsWith("data:")) {
              img?.scrollIntoView();
              setTimeout(() => loadImage(done), 33);
              return;
            }
          }
          done();
        }

        function scrollEachProduct(done: () => void, i = 0) {
          let es = document.querySelectorAll(".ProductItem");
          let e = es[i];
          if (!e) {
            done();
          } else {
            e.scrollIntoView();
            setTimeout(() => scrollEachProduct(done, i + 1), 33);
          }
        }

        function loadProduct(done: () => void) {
          let total = parseInt(
            document.querySelector(".product-total")?.textContent || ""
          );
          if (!total) {
            setTimeout(() => loadImage(() => loadProduct(done)), 33);
            return;
          }
          let es = document.querySelectorAll(".ProductItem");
          let count = es.length;
          scrollEachProduct(() => {
            function check() {
              let es2 = document.querySelectorAll(".ProductItem");
              let newCount = es2.length;
              console.log({ newCount });
              if (count != newCount) {
                loadImage(() => loadProduct(done));
                // es2[Math.floor(es2.length / 2)].scrollIntoView();
                // setTimeout(check, 33);
                return;
              }
              // check if all loaded
              let remind = total - newCount;
              if (remind < 41) {
                done();
              } else {
                loadImage(() => loadProduct(done));
              }
            }
            setTimeout(check, 33);
          });
        }

        function collectProduct() {
          let es = document.querySelectorAll(".ProductItem");
          let list: ProductItem[] = [];
          for (let root of Array.from(es)) {
            let name = root
              .querySelector(".name[title]")
              ?.getAttribute("title")
              ?.trim();
            let image_src =
              root.querySelector<HTMLImageElement>(".img img")?.src;
            let priceText =
              root.querySelector(".price .promotion")?.textContent ||
              root.querySelector(".price")?.textContent;
            let href = root.querySelector("a")?.href;

            if (!name || !image_src || !priceText || !href) {
              continue;
            }

            let price = +priceText.replace("$", "").trim();
            if (!price) {
              reject(new Error("failed to parse price"));
              continue;
            }
            list.push({ name, image_src, price, href });
          }
          return list;
        }

        loadProduct(() => {
          loadImage(() => {
            let list = collectProduct();
            resolve(list);
          });
        });
      });

      // function waitLoading(cb: () => void) {
      //   let div = document.querySelector(".loading.load_more");
      //   if (!div) {
      //     return;
      //   }
      //   let rect = div.getBoundingClientRect();
      //   if (rect.height == 0) {
      //     cb();
      //   } else {
      //     setTimeout(() => waitLoading(cb), 33);
      //   }
      // }

      // function scrollToLast(cb: () => void) {
      //   let es = document.querySelectorAll(".ProductItem");
      //   console.log("number of products:", es.length);
      //   let last = es[es.length - 1];
      //   last.scrollIntoView();
      //   setTimeout(() => {
      //     waitLoading(() => {
      //       let es2 = document.querySelectorAll(".ProductItem");
      //       if (es2.length != es.length) {
      //         return scrollToLast(cb);
      //       } else {
      //         cb();
      //       }
      //     });
      //   }, 500); //500
      // }

      // return new Promise<ProductItem[]>((resolve) => {
      //   scrollToLast(() => {
      //     let productList: Array<Promise<ProductItem | null>> = [];

      //     // document.querySelectorAll('.ProductTopSevenTabs .ProductItem').forEach(root => {
      //     document.querySelectorAll(".ProductItem").forEach((root) => {
      //       productList.push(
      //         new Promise<ProductItem | null>((resolve, reject) => {
      //           function loadImage() {
      //             // let name = root.querySelector('.name[title]')?.textContent?.trim()
      //             let name = root
      //               .querySelector(".name[title]")
      //               ?.getAttribute("title")
      //               ?.trim();
      //             let img = root.querySelector<HTMLImageElement>(".img img");
      //             let priceText =
      //               root.querySelector(".price .promotion")?.textContent ||
      //               root.querySelector(".price")?.textContent;
      //             let href = root.querySelector("a")?.href;
      //             // console.log({ name, priceText });

      //             if (!name || !img || !priceText || !href) {
      //               resolve(null);
      //               return;
      //             }

      //             // href = href_txt + href

      //             let price = +priceText.replace("$", "").trim();
      //             if (!price) {
      //               reject(new Error("failed to parse price"));
      //               return;
      //             }
      //             let image_src = img?.src;
      //             if (image_src.startsWith("data:")) {
      //               // console.log('image still loading?:', img);
      //               img.scrollIntoView();
      //               setTimeout(loadImage, 500);
      //             } else {
      //               resolve({ name, image_src, price, href });
      //             }
      //           }

      //           loadImage();
      //         })
      //       );
      //     });

      //     Promise.all(productList).then((maybeProductList) => {
      //       // let productList = maybeProductList =
      //       let productList = maybeProductList
      //         .filter((product) => product != null)
      //         .map((product) => product!);
      //       // console.log('resolve:', productList);
      //       resolve(productList);
      //     });
      //   });
      // });
    });
    return productList;
  }

  // async collectTop10(){
  //   let page = await this.browser.newPage();
  //   // let url = "https://www.ztore.com/tc/search/" + encodeURIComponent(keyword);
  //   let url = "https://www.ztore.com/tc/category/group/snacks"
  //       await page.goto(url);
  //   let productList = await this.collectProductList(page);
  //   console.log(productList);

  //   // return [];
  // }
}
